# Model-driven design of experiments for curcumin production by *Pseudomonas putida*
Code and models for the optimal experimental design related to the production of curcumin in *Pseudomonas putida*

## Contents:
- Libraries and utility scripts:
    - `symbolicsbml/` code related to model construction. Custom branch of [SymbolicSBML](https://gitlab.com/wurssb/Modelling/symbolicsbml).
    - `parameter-balancer/` code related to parameter balancing. Custom branch of [Parameter Balancer](https://gitlab.com/wurssb/Modelling/parameter-balancer).
    - `AmiciResultWrapper.py` utility script for accessing and plotting the data returned by amici simulations.
    - `pathway.py` utility script to create the model based on the active reactions and genes.
- Simulation of experiments:
    - `experimental_design.py` contains the simulation code related to the experimental design.
    - `experimental_design_data_based.py` contains the simulation code compared to the experimental measurements.
- Analysis and plotting code:
    - `analyse_data_fit.py` contains the analysis related to the experimental data.
    - `analyse_experimental_design.py` contains the analysis of the experimental designs.
    - `analyse_pulse_experiment_factors.py` contains the statistical analysis of round two of the experimental designs.
    - `plot_chemical_structures.py` contains the code to plot the chemical structure used for the pathway figure.
    - `search_equalibrator.py` contains the code to find the estimated chemical potential for all metabolites and their covariance.
- Model and data files:
    - `data/model.xlsx` defines the model and parameter values, and is used by `pathway.py` to create the sbml model. It contains several tabs describing the metabolites, reactions, and genes that are used the model. In addition, it contains the parameters and concentration measurements extracted from literature, as well as the thermodynamic estimates from eQuilibrator.
    - `data/` contains metabolomics and measured data used as reference.
- `example/` contains the constructed model file and the parameters used for the analysis. These files are generated automatically during the simulations, but are included here as an example without having to rerun the analysis.
- `enviroment.yml` contains the specification of dependencies for use with conda.

## Requirements:
The code was verified to work with the following versions on Ubuntu 20.04 LTS:
- python - 3.9.7
- amici - 0.11.21
- cobra - 0.22.1
- matplotlib - 3.5.0
- numpy - 1.21.4
- scipy - 1.7.3
- h5py - 3.6.0
- pandas - 1.3.4
- sympy - 1.8
- seaborn - 0.11.2
- python-libsbml - 5.19.0 
- equilibrator-api - 0.4.5.1
- networkx - 2.6.3
- pyDOE2 - 1.3.0
- pebble - 4.6.3
- statsmodels - 0.13.1
- rdkit - 2021.09.2

## Installation and usage instructions:
Installation is easiest using [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).
- `conda env create -f environment.yml -n curcumin` to install the required dependencies in a new environment (this might take a while).
- `conda activate curcumin` to activate the environment
- `pip install -e symbolicsbml` to install the included version of symbolicSBML.
- `pip install -e parameterbalancer` to install the included version of parameter balancer.

You can now run the included scripts to reproduce the required data files and to plot the figures. Note that the scripts should be run in the order below, as the first scripts generate the output required for the subsequent analysis.
- Running the analysis and plotting code requires running the simulations first. If needed, reduce the number of samples in the simulation scripts to decrease the running time.
- First run `experimental_design.py` and `experimental_design_data_based.py` to generate the model, parameter ranges, experimental design and simulation results.
- Next you can run `analyse_data_fit.py` or `analyse_experimental_design.py` to recreate the figures.
- Finally, `analyse_pulse_experiment_factors.py` can be used to recreate the statistical analysis of the second round of experiments.

When done, run:
- `conda deactivate` to exit the environment.
## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details.
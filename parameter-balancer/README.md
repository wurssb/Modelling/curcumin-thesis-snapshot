# Parameter Balancer
A python package to create thermodynamically valid parameter sets as shown in the paper of Lubitz *et al* [1].

Usage:
- Load or create a model using `symbolicSBML.SBMLModel`.
- Load Parameter data using the `ParameterData` object.
- Define priors for each parameter type.
- Create a balancer instance using `Balancer`, the data and the model structure.
- Obtain a `BalancedParameterData` object by calling `{Balancer}.balance()`
- The `BalancedParameterData` data object can be used in different ways:
	- Get the mean / median / standard deviation of the resulting distributions.
	- Sample complete parameter sets directly from the distributions using random or Latin Hypercube sampling.
	- Save and load the balanced parameters to disk for later use.
- See `balancer/balancer.py` for examples.

Note: For large models, the methods might require several gigabytes of available memory. Use with caution!

Note: Also check out Lubitz' recently published (online) [implementation](https://www.parameterbalancing.net) [2]

## Requirements:
- Python 3.4+ (Possibly 2.7+, untested)
- Numpy
- Scipy
- Pandas
- [symbolicSBML](https://gitlab.com/wurssb/Modelling/symbolicsbml)
- pyDOE (Optional: Required for latin hypercube sampling.)

## Installation:
- The easiest way to install `parameter_balancer` is via pip:
  1. First install symbolicSBML:
  `pip install git+https://gitlab.com/wurssb/Modelling/symbolicsbml.git#egg=symbolicSBML`
  2. Then install parameter-balancer:
  `pip install git+https://gitlab.com/wurssb/Modelling/parameter-balancer.git#egg=parameter-balancer`
- Alternatively:
    - Install `numpy`, `scipy`, `pandas` and `python-libsbml` via your preferred method.
    - optionally install `pyDOE` via your preferred method.
    - Clone the symbolicSBML repository: 
    	`git clone https://gitlab.com/wurssb/Modelling/symbolicsbml`
    - Run `python setup.py install` in the cloned repository to install symbolicSBML.
    - Clone the parameter_balancer repository:
    	`git clone https://gitlab.com/wurssb/Modelling/parameter-balancer`
    - Run `python setup.py install` in the cloned repository to install parameter-balancer.

## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details.

## References:
1. Lubitz, T., Schulz, M., Klipp, E., & Liebermeister, W. (2010).
    Parameter balancing in kinetic models of cell metabolism.
    The Journal of Physical Chemistry. B, 114(49), 16298-303.
    http://doi.org/10.1021/jp108764b
2. Lubitz, T., & Liebermeister, W. (2019). 
    Parameter balancing: consistent parameter sets for kinetic metabolic models. Bioinformatics. 
    https://doi.org/10.1093/bioinformatics/btz129

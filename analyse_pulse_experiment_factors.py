import itertools

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import statsmodels.formula.api as smf
import pathlib

base_dir = pathlib.Path("results_exp_pred")
plot_dir = pathlib.Path("results/exp_design_run/anova")
plot_dir.mkdir(exist_ok=True, parents=True)

pulse_trace_df = pd.read_csv(base_dir / "pulse_trace_df.csv", index_col=0)
singles_df = pd.read_csv(base_dir / "singles_trace_df.csv", index_col=0)

# The data has a few outliers on both sides, so we trim 1% on each side.
fig, axes = plt.subplots(2, 2)
trim = 1
qmin, qmax = pulse_trace_df.Trace.quantile([trim / 100, 1 - trim / 100])
outliers = ~((qmax > pulse_trace_df.Trace) & (pulse_trace_df.Trace > qmin))
sns.histplot(pulse_trace_df.Trace, ax=axes[0, 0])
sns.histplot(pulse_trace_df.Trace.loc[~outliers], ax=axes[0, 1])
sns.histplot(np.log(pulse_trace_df.Trace), ax=axes[1, 0])
sns.histplot(np.log(pulse_trace_df.Trace.loc[~outliers]), ax=axes[1, 1])
axes[0, 0].set_title("Untrimmed")
axes[0, 1].set_title("Trimmed ({}%)".format(trim))
axes[1, 0].set_xlabel("log(Trace)")
axes[1, 1].set_xlabel("log(Trace)")
sns.despine(fig)
fig.tight_layout()
fig.savefig(plot_dir / "pulse_trace_outliers.png", dpi=300)


model1 = smf.ols(
    formula="Trace ~ C(Base) + C(T_pulse, Diff) + C(M_pulse)", data=pulse_trace_df
)
result1 = model1.fit()
anova_table1 = sm.stats.anova_lm(result1)

model2 = smf.ols(
    formula="Trace ~ C(Base) * C(T_pulse, Diff) * C(M_pulse)", data=pulse_trace_df
)
result2 = model2.fit()
anova_table2 = sm.stats.anova_lm(result2)

model_o1 = smf.ols(
    formula="Trace ~ C(Base) + C(T_pulse, Diff) + C(M_pulse)",
    data=pulse_trace_df.loc[~outliers],
)
result_o1 = model_o1.fit()
anova_table_o1 = sm.stats.anova_lm(result_o1)

model_o2 = smf.ols(
    formula="Trace ~ C(Base) * C(T_pulse, Diff) * C(M_pulse)",
    data=pulse_trace_df.loc[~outliers],
)
result_o2 = model_o2.fit()
anova_table_o2 = sm.stats.anova_lm(result_o2)

plots = [
    (result1, "base"),
    (result2, "interactions"),
    (result_o1, "outliers"),
    (result_o2, "outliers_interactions"),
]

for result, name in plots:
    # Make qq plot to asses residuals
    fig, ax = plt.subplots()
    sm.qqplot(result.resid, fit=True, line="45", ax=ax)
    sns.despine(fig)
    fig.tight_layout()
    fig.savefig(plot_dir / "{}_qq_plot.png".format(name), dpi=300)
    # Make plots of residuals vs predictors.
    fig, axes = plt.subplots(3, 1, figsize=(9, 9))
    for variable, ax in zip(("Base", "M_pulse", "T_pulse"), axes.flat):
        sns.boxplot(
            y=result.resid,
            x=pulse_trace_df[variable],
            ax=ax,
        )
    sns.despine(fig)
    fig.tight_layout()
    fig.savefig(plot_dir / "{}_resid.png".format(name), dpi=300)

plt.close("all")


# Analysis of explained variance
factors = "C(Base)", "C(T_pulse, Diff)", "C(M_pulse)"

for name, sep in zip(("interactions", "linear"), (' * ', ' + ')):
    data = {}
    for f in itertools.chain.from_iterable(
        (i for i in itertools.combinations(factors, n)) for n in range(1, 5)
    ):
        model = smf.ols(
            formula="Trace ~ {}".format(sep.join(f)),
            data=pulse_trace_df.loc[~outliers],
        )
        result = model.fit()
        anova = sm.stats.anova_lm(result)
        anova = anova.assign(explained=anova.sum_sq / anova.sum_sq.sum())
        data[f + ("",) * (3 - len(f))] = anova.explained
    var = pd.DataFrame(data).sort_index(
        key=lambda x: (
            x.str.contains("Residual").astype(str) + x.str.count(":").astype(str) + x
        )
    )

    print((var * 100).round(1))
    print()

import numpy as np
import scipy.stats

# Adapted from https://gitlab.com/wurssb/model-reduction-for-targeted-kinetic-models/-/blob/master/pipeline/util.py#L259


def fit_distribution_from_ci(
    low,
    mean,
    high,
    distribution=scipy.stats.lognorm,
    weight=(1, 1, 1),
    start=(1.0, 1.0),
    interval=0.95,
):
    """Fit a distribution using a given confidence interval.

    :param low: Low end of the confidence interval
    :type low: float
    :param mean: Mean of the measurement
    :type low: float
    :param high: high end of the confidence interval
    :type high: float
    :param distribution: Type of distribution to fit, defaults to scipy.stats.lognorm
    :type distribution: scipy.stats distribution, optional
    :param start: Starting point of the optimization, defaults to (1, 1)
    :type start: tuple(float, float), optional
    :param interval: Width of the confidence interval being fitted, defaults to 0.95 (95% ci)
    :type interval: float, optional
    """
    data = np.array((low, mean, high))
    weight = np.array(weight)

    def error_f(x):
        """Calculate the error of fit for the distribution.

        :param x: Distribution size and scale (see scipy distributions)
        :type s: [float, float]
        :return: error of low end and high end of the distribution
        :rtype: [type]
        """
        if distribution == scipy.stats.lognorm:
            args = {"s": x[0], "scale": x[1]}
        elif distribution == scipy.stats.norm:
            args = {"loc": x[0], "scale": x[1]}
        low_fit, high_fit = distribution.interval(interval, **args)
        mean_fit = distribution.mean(**args)
        return (np.array((low_fit, mean_fit, high_fit)) - data) * weight

    result = scipy.optimize.least_squares(
        error_f, np.array(start), method="lm", ftol=1e-12, xtol=1e-12, gtol=1e-12
    )
    s, scale = result["x"]
    if distribution == scipy.stats.lognorm:
        args = {"s": s, "scale": scale}
    elif distribution == scipy.stats.norm:
        args = {"loc": s, "scale": scale}
    result.distribution = distribution(**args)
    result.ci_data = low, mean, high
    low_fit, high_fit = result.distribution.interval(interval)
    mean_fit = result.distribution.mean()
    result.ci_pred = (low_fit, mean_fit, high_fit)
    result.ci_error = (
        low - result.ci_pred[0],
        mean - result.ci_pred[1],
        high - result.ci_pred[2],
    )
    return result

# From Bennett et al. 2002
# Error in concentration measurements was determined by propagation of the
# uncertainty in R and L as standard errors in logarithmic space. The upper
# and lower bounds of the 95% confidence interval of the cellular metabolite
# concentration were calculated as previously described [26] (2):
# C_bound = exp(ln( C_avg ) ± 1.96 × SE_ln_C) (2)

# Thus we can fit with a lognormal distribution.

# MACOA
mean, low, high = 3.54e-05, 4.05e-7, 3.09e-3
res = fit_distribution_from_ci(
    low, mean, high, distribution=scipy.stats.lognorm, interval=0.95, weight=(1, 25, 1)
)
lowfit, meanfit, highfit = res.ci_pred
print("MACOA")
print(
    "Mean: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(mean, meanfit, abs(mean - meanfit))
)
print("Low : {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(low, lowfit, abs(low - lowfit)))
print(
    "High: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(high, highfit, abs(high - highfit))
)
print("{:.2e}, {:.2e}".format(meanfit, res.distribution.std()))

# AMET
mean, low, high = 1.84e-04, 1.19e-04, 2.84e-04
res = fit_distribution_from_ci(
    low, mean, high, distribution=scipy.stats.lognorm, interval=0.95, weight=(1, 25, 1)
)
lowfit, meanfit, highfit = res.ci_pred
print("AMET")
print(
    "Mean: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(mean, meanfit, abs(mean - meanfit))
)
print("Low : {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(low, lowfit, abs(low - lowfit)))
print(
    "High: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(high, highfit, abs(high - highfit))
)
print("{:.2e}, {:.2e}".format(meanfit, res.distribution.std()))

# FAD
mean, low, high = 1.73E-04, 9.33E-05, 3.19E-04
res = fit_distribution_from_ci(
    low, mean, high, distribution=scipy.stats.lognorm, interval=0.95, weight=(1, 25, 1)
)
lowfit, meanfit, highfit = res.ci_pred
print("FAD")
print(
    "Mean: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(mean, meanfit, abs(mean - meanfit))
)
print("Low : {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(low, lowfit, abs(low - lowfit)))
print(
    "High: {:.2e} / Fit: {:.2e} / Δ {:.2e}".format(high, highfit, abs(high - highfit))
)
print("{:.2e}, {:.2e}".format(meanfit, res.distribution.std()))

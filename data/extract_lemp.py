import pandas as pd

df = pd.read_excel(
    "lemp_metabolomics.xlsx",
    sheet_name="Supplementary Data 1",
    header=[0, 1, 2, 3],
    index_col=[0, 1, 2, 3],
)

timepoints = ["t0_1", "t0_2", "t0_3", "t0_4"]
metabolites = [
    "Acetyl-CoA",
    "ADP",
    "AMP",
    "ATP",
    "Coenzyme A",
    "Flavin adenine dinucleotide",
    "NAD",
    "NADH",
    "NADP",
    "NADPH",
    "Phenylalanine",
    "S-Adenosyl-homocysteine",
    "S-Adenosylmethionine",
    "Tyrosine",
]

# Select relevant data
columns = (
    df.columns.get_level_values(0) == "intracellular concentrations (µM)"
) & df.columns.get_level_values(2).isin(timepoints)
rows = df.index.get_level_values(1).isin(metabolites)

# Convert to float and drop missing rows
df = (
    df.loc[rows, columns]
    .apply(pd.to_numeric, errors="coerce")
    .dropna(axis=0, how="all")
)

# Summarize
df = (
    df.droplevel([0, -1, -2])
    .rename_axis(["metabolite"])
    .T.describe()
    .T[["mean", "std"]]
)

# Save to new sheet
df.to_excel("lemp_metabolomics_extracted.xlsx", sheet_name="Extracted")

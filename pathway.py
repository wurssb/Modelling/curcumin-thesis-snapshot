import collections
import pathlib
import warnings

import cobra
import networkx as nx
import numpy as np
import pandas as pd
import sympy

import parameter_balancer
import symbolicSBML
import symbolicSBML.rate_laws
from symbolicSBML import Parameters


def match_inchikey_charge(letter, charge):
    if letter < "A" or letter > "Z":
        raise ValueError("Invalid character")
    elif letter == "A":
        return charge < -12 or charge > 12
    else:
        return charge == (ord(letter) - ord("N"))


def _add_checked(self, other):
    """Add to the dictionairy, only if the key exists."""
    for key, value in other.items():
        if key in other:
            if isinstance(value, str):
                self[key].add(value)
            else:
                self[key] |= set(value)
        else:
            raise ValueError("Key does not exist: {} with values {}".format(key, value))


def parse_reaction_formula(substrates, products):
    """Parse the reaction formula into a dictionairy of metabolite: stoichiometry."""
    metabolites = {}
    for sign, reaction_formula in zip((-1, 1), (substrates, products)):
        for metabolite in reaction_formula.split(" + "):
            if " " in metabolite:
                count, metabolite = metabolite.split(" ")
            else:
                count = 1
            metabolites[metabolite] = sign * int(count)
    return metabolites


def parse_molecular_formula(formula):
    """Parse the molecular formula into a dictionairy of element: count."""
    return {
        k: int(v) if v else 1
        for k, v in cobra.core.metabolite.element_re.findall(formula)
        if v != "0"
    }


def to_networkx_bipartite(stoichiometry):
    # Convert to edgelist format
    df = (
        stoichiometry.unstack()
        .reset_index()
        .set_axis(["source", "target", "stoichiometry"], axis=1)
    )

    # Remove 0 entries
    df = df[df.stoichiometry != 0]

    # Swap edge direction at negative stoichiometries
    swap = df[df.stoichiometry < 0]
    df.loc[swap.index, "source"] = swap.target.values
    df.loc[swap.index, "target"] = swap.source.values
    df.stoichiometry = df.stoichiometry.abs()

    return nx.from_pandas_edgelist(
        df, "source", "target", True, create_using=nx.DiGraph
    )


class Pathway:
    # class variable name, excel sheet name, set index to ID column yes/no.
    pathway_overview_tabs = (
        ("metabolite_df", "Metabolites", True),
        ("reaction_df", "Reactions", True),
        ("gene_df", "Genes", True),
        ("thermodynamic_df", "Thermodynamics", True),
        ("thermodynamic_df_cov", "Thermodynamics_covariance", True),
        ("parameter_df", "Parameters", False),
        ("concentration_df", "Concentrations", False),
        ("regulator_df", "Regulators", False),
        ("conditions_df", "Conditions", True),
    )

    def __init__(
        self, pathway_information_path, active, ignore, priors, simplifications=None
    ):
        default_simplifcations = {
            # Simplify the enzyme * rate constants into one constant for enzymes that catalyze
            # only a single reaction.
            "enzyme_rate": True,
            # Fix parameters as constants
            "constants": dict(),
            # States that should not be in equations, except for delta mu.
            "implict_states": set(),
            # Fix states as constant
            "fixed_states": set(),
            # Fix reactions as constant (or other expression)
            "fixed_reactions": dict(),
        }
        if simplifications is None:
            self.simplifications = default_simplifcations
        else:
            default_simplifcations.update(**simplifications)
            self.simplifications = default_simplifcations

        self._load_pathway_overview(pathway_information_path)

        self.priors = priors
        self.T = self.conditions_df.loc["Temperature", "Value"]
        if "T" in self.simplifications["constants"]:
            self.simplifications["constants"]["T"] = self.T
        self.R = 8.3145 / 1000.0  # J mol-1 K-1 -> J mM-1 K-1
        if "R" in self.simplifications["constants"]:
            self.simplifications["constants"]["R"] = self.R

        # Active genes will cause catalyzed reactions to be added automatically.
        # Additional reactions can be added manually as well.
        self.input_active = {
            "genes": set(),
            "reactions": set(),
        }
        _add_checked(self.input_active, active)
        # Ignored reactions will NOT be added, even if the gene is active.
        self.input_ignore = {
            "reactions": set(),
        }
        _add_checked(self.input_ignore, ignore)

        self.gene_variants = active["genes"]
        (
            self.genes,
            self.reactions,
            self.metabolites,
            self.stoichiometry,
        ) = self.get_active_sets()

        self.verify()
        self.build_model()
        self.build_parameters()

    def _load_pathway_overview(self, path):
        """Load the different parts of the pathway."""
        for key, tab, set_ID in self.pathway_overview_tabs:
            df = (
                pd.read_excel(path, sheet_name=tab)
                .dropna(axis=1, how="all")
                .dropna(axis=0, how="all")
            )
            if set_ID:
                df = df.set_index("ID")
            self.__dict__[key] = df

    def save_pathway_overview(self, path):
        """Save the pathway overview excel file."""
        with pd.ExcelWriter(path) as writer:
            for key, tab in self.pathway_overview_tabs:
                self.__dict__[key].to_excel(sheet_name=tab)

    def verify(self):
        """Do a number of checks to catch errors in the input."""
        self.verify_metabolite_formulas()
        self.verify_reaction_balances()
        self.verify_network()

    def verify_metabolite_formulas(self):
        """Check whether the metabolite formulas exists and are consistent."""
        for ID, row in self.metabolite_df.iterrows():
            if ID not in self.metabolites:
                continue
            if pd.isna([row.Formula, row.Charge, row.InChiKey, row.InChi]).any():
                raise ValueError("Metabolite {} missing fields".format(ID))

            # H+ is special cased, it has no formula sublayer or correct charge inchikey.
            if row.Formula == "H":
                continue

            if not match_inchikey_charge(row.InChiKey[-1], row.Charge):
                raise ValueError(
                    "Metabolite {} charge does not match InChiKey".format(ID)
                )

            atoms = {
                k: v
                for k, v in parse_molecular_formula(row.Formula).items()
                if k != "H"
            }
            atoms_inchi = {
                k: v
                for k, v in parse_molecular_formula(row.InChi.split("/", 2)[1]).items()
                if k != "H"
            }
            if atoms != atoms_inchi:
                raise ValueError(
                    "Metabolite {} does not match inchi formula".format(ID)
                )

    def verify_reaction_balances(self):
        """Check whether all reactions are elementally and charge balanced."""
        for ID, row in self.reaction_df.iterrows():
            if ID not in self.reactions:
                continue
            try:
                stoichiometry = parse_reaction_formula(row.Substrates, row.Products)
            except ValueError:
                raise ValueError(
                    "Could not parse formula ({}): {} -> {}".format(
                        ID, row.Substrates, row.Products
                    )
                )

            atom_count = collections.defaultdict(int)
            for metabolite, count in stoichiometry.items():
                # Add elements and charge to sum
                atom_count["charge"] += (
                    count * self.metabolite_df.loc[metabolite].Charge
                )

                for element, element_count in parse_molecular_formula(
                    self.metabolite_df.loc[metabolite].Formula
                ).items():
                    atom_count[element] += count * (
                        int(element_count) if element_count else 1
                    )
            # Remove zeros
            atom_count = {k: v for k, v in atom_count.items() if v != 0}
            if atom_count:
                raise ValueError("Reaction {} imbalanced: {}".format(ID, atom_count))

    def verify_network(self):
        """Check for isolated subgraphs in the bipartite network."""
        G = to_networkx_bipartite(self.stoichiometry)
        subgraphs = sorted(nx.weakly_connected_components(G), key=len, reverse=True)
        if len(subgraphs) > 1:
            raise ValueError("Mutiple subgraphs detected {}".format(subgraphs))

    def get_active_sets(self):
        """Get the set of active reactions and metabolites."""
        # Find relevant genes
        genes = set(
            self.gene_df.loc[
                self.gene_df.index.isin(self.input_active["genes"])
            ].Gene.unique()
        )

        # Check which reactions should be active.
        reactions = self.reaction_df.loc[
            (
                # Implied by active gene
                self.reaction_df.Gene.isin(genes)
                # Manually marked as active
                | self.reaction_df.index.isin(self.input_active["reactions"])
            )
            # Manually marked as not active
            & ~self.reaction_df.index.isin(self.input_ignore["reactions"])
        ]

        # Find all metabolites in the active reactions.
        stoichiometry = {}
        for ID, reaction in reactions.iterrows():
            reaction_stoichiometry = parse_reaction_formula(
                reaction.Substrates, reaction.Products
            )
            stoichiometry[ID] = reaction_stoichiometry

        stoichiometry = (
            pd.DataFrame(stoichiometry)
            .fillna(0)
            .rename_axis(index="metabolites", columns="reactions")
            .sort_index(axis=0)
            .sort_index(axis=1)
            .convert_dtypes(convert_string=False, convert_boolean=False)
        )

        # Warn if there are stilll floats, this might give issues later in the model.
        if not stoichiometry.dtypes.map(pd.api.types.is_integer_dtype).all():
            warnings.warn("Floats in stoichiometry.")

        # TODO: Add regulators
        regulators = set()
        metabolites = set(stoichiometry.index) | regulators

        return genes, set(reactions.index), metabolites, stoichiometry

    def _get_aspecific_enzyme_interactions(self):
        shared_enzyme = {}
        for enzyme, reactions in self.reaction_df.loc[self.reactions].groupby("Gene"):
            if enzyme == None or len(reactions) <= 1:
                for reaction in reactions.index:
                    shared_enzyme[reaction] = None
            else:
                # Gather all metabolites combinations for this enzyme
                all_metabolites = []
                for ID, reaction_info in reactions.iterrows():
                    all_metabolites.append(
                        parse_reaction_formula(
                            reaction_info.Substrates, reaction_info.Products
                        )
                    )
                # Add all but your own metabolites.
                for i, reaction in enumerate(reactions.index):
                    other_reactions = all_metabolites[:i] + all_metabolites[i + 1 :]
                    shared_enzyme[reaction] = other_reactions
        return shared_enzyme

    def build_model(self):
        """Build the dynamic model using the current structure and settings."""
        ignore_metabolite_in_rate_law = []
        for metabolite in self.simplifications["implicit_states"]:
            ignore_metabolite_in_rate_law.append((metabolite, "binding"))
            ignore_metabolite_in_rate_law.append((metabolite, "thermodynamics_c"))

        shared_enzyme = self._get_aspecific_enzyme_interactions()

        # Create rate laws
        self.symbolics = {}
        for reaction in self.reactions:
            if reaction in self.simplifications["fixed_reactions"]:
                expression = sympy.sympify(
                    self.simplifications["fixed_reactions"][reaction]
                )
            else:
                reaction_info = self.reaction_df.loc[reaction]
                metabolites = parse_reaction_formula(
                    reaction_info.Substrates, reaction_info.Products
                )

                # TODO: Allow customization per reaction
                flux = symbolicSBML.rate_laws.modular_rate_law(
                    reaction=reaction,
                    metabolites=metabolites,
                    parametrisation="weg",
                    rate_law="CM",
                    regulation=None,
                    shared_enzyme=shared_enzyme[reaction],
                    cooperativitiy=1,
                    substitutions={"enzyme_rate"}
                    if (
                        self.simplifications["enzyme_rate"]
                        and not shared_enzyme[reaction]
                    )
                    else None,
                    set_assumptions=False,
                    return_parts=False,
                    enzyme=reaction_info["Gene"],
                    ignore_participants=ignore_metabolite_in_rate_law,
                )
            self.symbolics[reaction] = flux

        all_symbols = set().union(*(i.free_symbols for i in self.symbolics.values()))
        # Check that we didn't create multiple symbols with the same names but different asssumptions
        assert len(all_symbols) == len({i.name for i in all_symbols})
        # Mapping from parameter name to sympy symbol, to preserve any assumptions set on the symbols.
        self.symbols = {i.name: i for i in all_symbols}

        if self.simplifications["constants"]:
            # Since we might have set assumptions we need to find the right symbol and can't
            # match using strings
            self.constants = {
                self.symbols[k]: v for k, v in self.simplifications["constants"].items()
            }
            for reaction in self.symbolics:
                self.symbolics[reaction] = self.symbolics[reaction].subs(self.constants)
            # Remove the symbols
            for symbol in self.constants:
                all_symbols.remove(symbol)

        self.parameters = sorted(
            i.name for i in all_symbols if i.name not in self.stoichiometry.index
        )

        # Create modified stoichiometry matrix, removing fixed and implicit states.
        self.stoichiometry_prime = self.stoichiometry.copy()
        self.stoichiometry_prime = self.stoichiometry_prime.loc[
            ~self.stoichiometry_prime.index.isin(
                set(self.simplifications["fixed_states"])
                | set(self.simplifications["implicit_states"])
            ),
            :,
        ]
        # Substitue the metabolite identifier with a parameter identifier
        replacements = {}
        for state in self.simplifications["fixed_states"]:
            parameter = Parameters.join(Parameters.c, metabolite=state)
            new_symbol = sympy.Symbol(parameter)
            self.parameters.append(parameter)
            replacements[self.symbols[state]] = new_symbol
            # Add new symbol and remove old symbol.
            self.symbols[parameter] = new_symbol
            del self.symbols[state]

        for reaction in self.symbolics:
            self.symbolics[reaction] = self.symbolics[reaction].subs(replacements)

        self.symbolic_model = symbolicSBML.SymbolicModel(
            list(self.stoichiometry_prime.index),
            list(self.stoichiometry_prime.columns),
            parameters=self.parameters,
            stoichiometry=self.stoichiometry_prime.to_numpy(),
            fluxes=self.symbolics,
        )

    def build_parameters(self):
        """Build the parameter balancer using the current structure and settings."""
        # Create ParameterData object from parameters, regulators and thermodynamics tabs.
        df_mu = (
            self.thermodynamic_df.loc[
                self.thermodynamic_df.index.isin(self.metabolites),
                ["Mu", "Stdev"],
            ]
            .reset_index()
            .set_axis(["metabolite", "mean", "sd"], axis=1)
            .assign(parameter_type=Parameters.mu, reaction=np.NaN)
        )

        df_c = (
            self.concentration_df.loc[
                self.concentration_df.ID.isin(self.metabolites),
                ["ID", "Value", "Stdev"],
            ]
            .set_axis(["metabolite", "mean", "sd"], axis=1)
            .assign(parameter_type=Parameters.c, reaction=np.NaN)
        )

        df_par = self.parameter_df.loc[
            self.parameter_df.Gene.isin(self.gene_variants)
            & self.parameter_df.Parameter.isin(Parameters.all_balanced_parameters)
            & (
                self.parameter_df.Metabolite.isin(self.metabolites)
                | self.parameter_df.Metabolite.isna()
            )
            & (
                self.parameter_df.Reaction.isin(self.reactions)
                | self.parameter_df.Reaction.isna()
            ),
            ["Reaction", "Metabolite", "Parameter", "Value", "Stdev"],
        ].set_axis(["reaction", "metabolite", "parameter_type", "mean", "sd"], axis=1)

        # Add covariance data
        cov = []
        if not self.thermodynamic_df_cov.empty:
            for (met1, met2), value in (
                # Only take the upper triangle, excluding the diagional.
                self.thermodynamic_df_cov.where(
                    np.triu(np.ones_like(self.thermodynamic_df_cov), k=1) > 0
                )
                .stack()
                .iteritems()
            ):
                if met1 in self.metabolites and met2 in self.metabolites:
                    cov.append(
                        (
                            (Parameters.mu, met1, None),
                            (Parameters.mu, met2, None),
                            value,
                        )
                    )
        if not cov:
            cov = None

        parameter_data = parameter_balancer.ParameterData.from_dataframe(
            pd.concat([df_mu, df_c, df_par]).reset_index(drop=True), cov=cov
        )
        self._used_parameter_data = parameter_data
        # We need to use the model here with no fixed concentrations to have
        # the original stoichiometric matrix for balancing.
        symbolic_model = symbolicSBML.SymbolicModel(
            list(self.stoichiometry.index),
            list(self.stoichiometry.columns),
            parameters=self.parameters,
            stoichiometry=self.stoichiometry.to_numpy(),
            fluxes=self.symbolics,
        )

        self._balancer = parameter_balancer.Balancer(
            priors=self.priors,
            data=parameter_data,
            structure=symbolic_model.to_sbml_model(),
            T=self.T,
            augment=False,
            cooperativities=None,
        )
        self.balanced_parameters = self._balancer.balance()


if __name__ == "__main__":
    path = pathlib.Path("data/model.xlsx")

    putida_native_genes = {"fcs", "ech", "curA", "vdh"}
    knockouts = {"ech", "curA", "vdh"}
    knockins = {"CURS1", "DCS", "C3H", "COMT", "CCoAOMT"}

    active_genes = (putida_native_genes - knockouts) | knockins
    active_reactions = set()
    ignore_reactions = {
        # Everything cinnamic acid for now.
        "FCL_CIA",
        "COAHY_CIA",
        "DCS_CICOA",
        "CURS_DCIM",
        "CURS_CIFEM1",
        "CURS_CIFEM2",
        "CURS_CICUM1",
        "CURS_CICUM2",
    }

    # What is the best way to fix things AND simplify the equations?
    fixed = {
        "CO2",
        "O2",
        "ATP",
        # "ADP",
        "AMP",
        "PPI",
        "H",
        "H2O",
        # "NH3",
        "COA",
        "MACOA",
        "FAD",
        "FADH",
        "AMET",
        "AHCYS",
        "FAD",
        "FADH",
    }

    # priors = {
    #     # Base quantities
    #     Parameters.mu: (-880.0, 680.00),
    #     Parameters.kv: (10.0, 6.26),
    #     Parameters.km: (0.1, 6.26),
    #     Parameters.c: (0.1, 10.32),
    #     Parameters.u: (0.0001, 10.32),
    #     Parameters.ki: (0.1, 6.26),
    #     Parameters.ka: (0.1, 6.26),
    #     # Derived quantities
    #     Parameters.keq: (1.0, 10.32),
    #     Parameters.kcat_prod: (10.0, 10.32),
    #     Parameters.kcat_sub: (10.0, 10.32),
    #     Parameters.vmax: (0.001, 17.01),
    #     Parameters.A: (0.0, 10.00),
    #     Parameters.mu_p: (-880.0, 680.00),
    # }
    priors = {
        # Base quantities
        Parameters.mu: (-880.0, 680.00),
        Parameters.kv: (10.0, 6.26),
        Parameters.km: (0.1, 3),
        Parameters.c: (0.1, 3),
        Parameters.u: (0.0001, 3),
        Parameters.ki: (0.1, 3),
        Parameters.ka: (0.1, 3),
        # Derived quantities
        Parameters.keq: (1.0, 10.32),
        Parameters.kcat_prod: (10.0, 10.32),
        Parameters.kcat_sub: (10.0, 10.32),
        Parameters.vmax: (0.001, 17.01),
        Parameters.A: (0.0, 10.00),
        Parameters.mu_p: (-880.0, 680.00),
    }
    T = 300

    p = Pathway(
        pathway_information_path=path,
        active={"genes": active_genes, "reactions": active_reactions},
        ignore={"reactions": ignore_reactions},
        priors=priors,
        T=T,
        simplifications={
            "enzyme_rate": True,
            # Fix parameters as constants
            "constants": {"R": None, "T": None},
            # Fix states as constant
            "fixed_states": fixed,
            # Fix reactions as constant (or other expression)
            "fixed_reactions": dict(),
        },
    )
    # TODO: Merge u + kv where possible to avoid identifiability issues.
    #   - all enzymes with unique reactions.
    #   - mu of cofactor pairs
    #   - concentration of cofactor pairs
    #   - kv DCUR1 == kv DCUR2 (Is it?)

    # TODO: Allow other kinetics to be added
    # kcat -> kv or kcatprod?
    # km/kcat shared over multiple reactions?
    parameters = p.balanced_parameters.to_frame(only_base=False).T

    sbml_model = p.symbolic_model.to_sbml_model()
    sbml_model.initial_concentrations = {
        i: parameters.loc[pd.IndexSlice[Parameters.c, i, :], "median"][0]
        for i in sbml_model.metabolites
    }
    sbml_model.parameter_values = {i: 1 for i in sbml_model.parameters}
    sbml_model.save("test.sbml")

import collections
import pathlib
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

base = pathlib.Path("results_run_data")
plot_dir = pathlib.Path("results/data_run")
plot_dir.mkdir(exist_ok=True, parents=True)

limit_samples = None

evaluate_fit = False
plot_fit = False
plot_exp_comparison = False

# Read in parameters
parameter_sets = pd.read_csv(base / "parameters.csv", index_col=0)
n_samples = (
    len(parameter_sets)
    if not limit_samples
    else min(limit_samples, len(parameter_sets))
)

# Read in experimental design.
design = pd.read_csv(base / "experimental_design.csv", header=[0, 1], index_col=0)

# Read in relevant experimental data.
experimental_data_file = pathlib.Path("data/measurement_oct_12_cleaned.xlsx")
with pd.ExcelFile(experimental_data_file) as file:
    strains = file.parse(sheet_name="strains", header=0).set_index(
        ["strain", "location"]
    )
    experiments = file.parse(sheet_name="experiments", header=0).set_index("experiment")
    measurements = file.parse(sheet_name="measurements", header=0).set_index(
        ["experiment", "strain"]
    )

# We ignore the experiments with TAL/C3H or glc/tyr as substrates,
# as well as the experiments not using CURS1.
measurements = measurements.loc[
    ~(
        measurements.index.get_level_values("experiment").str.startswith("Glc")
        | measurements.index.get_level_values("experiment").str.startswith("Tyr")
    )
    & measurements.index.get_level_values("strain").str.contains("CURS1")
]
experiments = experiments.drop("TYR", axis=1)

t_final = 250

# Add design index to measurements table.
measurements = (
    pd.merge(
        measurements.reset_index(),
        design.name.rename_axis("design_idx").reset_index(),
        how="inner",
    )
    .rename(columns={"sample": "experimental_sample"})
    .assign(time=t_final)
)

# Load data and extract only the relevant bits.
simulated_measurements = collections.defaultdict(list)
for path in sorted((base / "simulations").iterdir()):
    match = re.match(r"s(?P<sample>\d+)e(?P<experiment>\d+).h5", path.name)
    if match is not None:
        sample = int(match.group("sample"))
        if limit_samples is not None and sample >= limit_samples:
            continue
        experiment = int(match.group("experiment"))
        with pd.HDFStore(path, "r") as store:
            y = store["data/y"]

        selection = measurements.loc[measurements.design_idx == experiment]
        simulated_measurements[sample].append(
            pd.DataFrame(
                y.lookup(selection.time, selection.metabolite),
                index=selection.index,
                columns=[sample],
            )
        )
simulated_measurements = pd.concat(
    [pd.concat(v).sort_index(axis=0) for v in simulated_measurements.values()], axis=1
).sort_index(axis=1)

experimental_measurements = measurements.set_index(
    ["design_idx", "metabolite", "experimental_sample"]
)["concentration"]

simulated_measurements = simulated_measurements.set_index(
    experimental_measurements.index
).rename_axis("parameter_sample", axis=1)

# Plot some simulations with the data.
points = {}
for path in sorted((base / "simulations").iterdir()):
    match = re.match(r"s(?P<sample>\d+)e(?P<experiment>\d+).h5", path.name)
    if match is not None:
        sample = int(match.group("sample"))
        if limit_samples is not None and sample >= limit_samples:
            continue
        experiment = int(match.group("experiment"))
        with pd.HDFStore(path, "r") as store:
            points[sample, experiment] = store["data/x"]
points = pd.concat(points, names=["sample", "experiment"])

groups = [
    ("Substrates", ["CUA", "FEA", "CAA"], (1e-6, 2.01)),
    ("CoA derivatives I", ["CUCOA", "FECOA", "CACOA"], (1e-3, 2.01)),
    ("CoA derivatives II", ["DCUCOA", "DFECOA"], (1e-4, 1e-1)),
    ("Curcuminoids", ["CUR", "DCUR", "BDCUR"], (1e-3, 1.01)),
]

style = {
    "Substrates": {
        "CUA": "r",
        "FEA": "g",
        "CAA": "b",
    },
    "CoA derivatives I": {
        "CUCOA": "r",
        "FECOA": "g",
        "CACOA": "b",
    },
    "CoA derivatives II": {
        "DCUCOA": "r",
        "DFECOA": "g",
    },
    "Curcuminoids": {
        "CUR": "r",
        "DCUR": "g",
        "BDCUR": "b",
    },
}
alpha = 0.10
plot_samples = [
    # (np.log(error.mean()).sort_values().head(100).index, "top"),
    (np.random.choice(np.arange(0, n_samples, 1), 100), "random"),
    # (np.arange(0, n_samples, 1), "all"),
]
include_measurements = False
skip_experiments = [0, 1, 2, 4, 5, 6]
for samples, sample_set in plot_samples:
    for reference_experiment in design.index:
        if reference_experiment in skip_experiments:
            continue
        experiment, strain = design.loc[reference_experiment, pd.IndexSlice["name", :]]
        fig, axes = plt.subplots(2, 2, sharex=True)
        for ax, (group, metabolites, yrange) in zip(axes.flat, groups):
            for sample in samples:
                points.loc[
                    pd.IndexSlice[sample, reference_experiment, :], metabolites
                ].reset_index(level=[0, 1], drop=True).plot(
                    ax=ax,
                    alpha=alpha,
                    style=style[group],
                )
            if include_measurements:
                for metabolite in metabolites:
                    relevant = measurements.loc[
                        (measurements.experiment == experiment)
                        & (measurements.strain == strain)
                        & (measurements.metabolite == metabolite)
                    ]
                    if not relevant.empty:
                        relevant = relevant[["concentration", "time", "metabolite"]]
                        ax.scatter(
                            x=relevant.time,
                            y=relevant.concentration,
                            c=relevant.metabolite.map(style[group]),
                            # Allow them to go over the edge of the plot.
                            clip_on=False,
                        )
            lgd = ax.legend(metabolites, frameon=False, title=group)
            _ = [line.set_alpha(0.75) for line in lgd.get_lines()]
            ax.set_xlim(0, t_final + t_final // 100)
            ax.set_ylim(*yrange)
            ax.set_yscale("log")

            if group in ('CoA derivatives II', 'Curcuminoids'):
                ax.set_xlabel('Time (a.u.)')
            else:
                ax.set_xlabel(None)

            if group in ('CoA derivatives II', 'Substrates'):
                ax.set_ylabel('Concentration (mM)')
            else:
                ax.set_ylabel(None)

        sns.despine(fig)
        fig.suptitle(
            "Experiment: {}".format(
                experiment,
            )
        )
        fig.set_size_inches(8, 8)
        fig.tight_layout()
        fig.savefig(
            plot_dir / "exp_{}_{}.png".format(experiment, sample_set),
            dpi=300,
        )
        plt.close(fig)

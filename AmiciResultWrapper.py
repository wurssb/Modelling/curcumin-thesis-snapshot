from functools import cached_property
import itertools
import subprocess

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import seaborn as sns
import symbolicSBML


class AmiciResultWrapper:
    """Wrapper for amici ReturnDataView object.

    Allows for:
        - serialization to h5 data file.
        - deserialization from h5 data file.
        - calculation of:
            - normalized state and observable sensitivity
            - normalized state and observable sensitivity correlation
            - FIM observed states and timepoints
        - plotting of:
            - observable / state trajectories
            - (normalized) sensitivity trajectories
            - FIM
            - (normalized) sensitivity correlation matrix
            - graph structure of correlated parameters
            - (normalized) pairwise sensitivity of two parameters w.r.t. different observables.

    # TODO: Allow for some manner of lazy loading for high-throughput analysis
    #       of a subset of the data?
    """

    qualitative_cmap = "muted"
    sequential_cmap = "crest"
    divergent_cmap = "vlag"

    linestyles = itertools.cycle(["solid", "dashed", "dotted", "dashdot", (0, (5, 10))])
    linecolors = itertools.cycle(sns.color_palette(qualitative_cmap))

    plot_size = 10, 6
    square_plot_size = 12, 10

    def __init__(self, rdata, model, t_measured=None):
        self.parameters = pd.Series(model.getParameterNames(), name="parameter")
        self.states = pd.Series(model.getStateNames(), name="states")
        self.observables = pd.Series(model.getObservableNames(), name="observable")

        self.t = pd.Series(rdata.ts, name="time")
        if t_measured:
            if not set(t_measured).issubset(set(rdata.ts)):
                raise ValueError(
                    "The measured time points have to be a subset of the simulated time points."
                )
            self.t_prime = pd.Series(t_measured, name="measurements")
        else:
            self.t_prime = self.t

        self.p = pd.Series(
            model.getParameters(), index=self.parameters, name="parameter_value"
        )
        self.y = pd.DataFrame(rdata.y, index=self.t, columns=self.observables)
        self.x = pd.DataFrame(rdata.x, index=self.t, columns=self.states)
        self.sy = pd.DataFrame(
            rdata.sy.flat,
            index=pd.MultiIndex.from_product(
                [self.t, self.parameters, self.observables],
            ),
            columns=["sensitivity"],
        )
        self.sx = pd.DataFrame(
            rdata.sx.flat,
            index=pd.MultiIndex.from_product(
                [self.t, self.parameters, self.states],
            ),
            columns=["sensitivity"],
        )

        # General diagnostics
        self.diagnostics = pd.Series(
            {
                "cpu_time": rdata.cpu_time,
                "status": rdata.status,
            }
        )
        # Diagnostics per time step.
        self.timed_diagnostics = pd.DataFrame(
            {
                "numerrtestfails": rdata.numerrtestfails,
                "numnonlinsolvconvfails": rdata.numnonlinsolvconvfails,
                "numrhsevals": rdata.numrhsevals,
                "numsteps": rdata.numsteps,
                "order": rdata.order,
            },
            index=self.t,
        )

    def to_hdf(self, path, compress=True):
        # Compression does not work with fixed tables, but the ptrepack from
        # pytables works fine and results in pandas readable files.
        with pd.HDFStore(path, "w") as store:
            put = lambda name, df: store.put(
                name,
                df,
                format="fixed",
                # complib="blosc:lz4",
                # complevel=9,
                track_times=False,
            )
            # Save index names and timepoints.
            put("index/parameters", self.parameters)
            put("index/states", self.states)
            put("index/observables", self.observables)
            put("index/t", self.t)
            put("index/t_prime", self.t_prime)

            # Save parameter values, simulated trajectories and sensivities.
            put("data/p", self.p)
            put("data/y", self.y)
            put("data/x", self.x)
            put("data/sy", self.sy)
            put("data/sx", self.sx)

            # Save diagnostics.
            put("diagnostics/diagnostics", self.diagnostics)
            put("diagnostics/timed_diagnostics", self.timed_diagnostics)

        if compress:
            # Move
            temp = path.with_suffix(path.suffix + ".temp")
            path.rename(temp)

            # Compress
            try:
                subprocess.check_call(
                    [
                        "ptrepack",
                        "--complevel=9",
                        "--complib=blosc:lz4",
                        str(temp),
                        str(path),
                    ]
                )
            except subprocess.CalledProcessError:
                # Move original file back
                temp.rename(path)

            # Remove temporary file.
            temp.unlink()

    @classmethod
    def from_hdf(cls, path, hdf_path=None):
        self = cls.__new__(cls)
        if hdf_path is None:
            hdf_path = ""
        elif not hdf_path.endswith("/"):
            hdf_path += "/"

        with pd.HDFStore(path, "r") as store:
            self.parameters = store[hdf_path + "index/parameters"]
            self.states = store[hdf_path + "index/states"]
            self.observables = store[hdf_path + "index/observables"]
            self.t = store[hdf_path + "index/t"]
            self.t_prime = store[hdf_path + "index/t_prime"]

            self.p = store[hdf_path + "data/p"]
            self.y = store[hdf_path + "data/y"]
            self.x = store[hdf_path + "data/x"]
            self.sy = store[hdf_path + "data/sy"]
            self.sx = store[hdf_path + "data/sx"]

            self.diagnostics = store[hdf_path + "diagnostics/diagnostics"]
            self.timed_diagnostics = store[hdf_path + "diagnostics/timed_diagnostics"]
        return self

    # TODO: Would be more efficient to assign this as a column to sy?
    @cached_property
    def sy_norm(self):
        return (
            self.sy
            * (
                self.p.values[np.newaxis, :, np.newaxis]
                / self.y.values[:, np.newaxis, :]
            ).flatten()[:, np.newaxis]
            # Since y can be 0, we can get a divide by zero. In this cases the sensitivity is
            # clearly 0, so we can fill it.
        ).fillna(0)

    @cached_property
    def sy_corr(self):
        return self.sy.unstack("parameter").corr().droplevel(0, 0).droplevel(0, 1)

    @cached_property
    def sy_norm_corr(self):
        return self.sy_norm.unstack("parameter").corr().droplevel(0, 0).droplevel(0, 1)

    @cached_property
    def sx_norm(self):
        return (
            self.sx
            * (
                self.p.values[np.newaxis, :, np.newaxis]
                / self.x.values[:, np.newaxis, :]
            ).flatten()[:, np.newaxis]
        ).fillna(0)

    @cached_property
    def sx_corr(self):
        return self.sx.unstack("parameter").corr().droplevel(0, 0).droplevel(0, 1)

    @cached_property
    def sx_norm_corr(self):
        return self.sx_norm.unstack("parameter").corr().droplevel(0, 0).droplevel(0, 1)

    @cached_property
    def FIM(self):
        sy_prime = self.sy.loc[self.t_prime].unstack("parameter").fillna(0)
        return sy_prime.T.dot(sy_prime)

    @cached_property
    def parameter_display_names(self):
        return self.parameters.apply(
            lambda x: symbolicSBML.Parameters.to_latex(
                x, include=("parameter", "reaction", "enzyme", "metabolite")
            )
        )

    def plot_trajectories(self, observables=True, pathway=None):
        """Plot the observables/states over time."""
        if observables:
            labels = self.observables
            points = self.y
        else:
            labels = self.states
            points = self.x

        if pathway:
            style_groups = True
            names = (
                (pathway["metabolites"].set_index("ID").loc[labels])
                .reset_index()
                .sort_values("Group")
            )
            linestyles = dict(
                zip(
                    names.Group.unique(),
                    self.linestyles,
                )
            )
            iterator = names.iterrows()
        else:
            style_groups = False
            iterator = enumerate(labels)

        fig, ax = plt.subplots()
        for (i, row), color in zip(iterator, self.linecolors):
            # Plot lines
            ax.plot(
                self.t,
                points.loc[self.t, row.ID if style_groups else row],
                linestyle=linestyles[row.Group] if style_groups else "solid",
                color=color,
                label=row.ID if style_groups else row,
            )
            # Overlay markers only at measurement points
            ax.plot(
                self.t_prime,
                points.loc[self.t_prime, row.ID if style_groups else row],
                marker="x",
                linewidth=0.0,
                color=color,
                label=None,
            )

        ax.set_xlim(*self._axis_range(self.t, close="right"))
        ax.set_ylim(*self._axis_range(points))
        ax.set_xlabel("Time (s)")
        ax.set_ylabel("Concentration (mM)")

        ax.legend(frameon=False)
        # ax.set_title(self.experiment)

        fig.set_size_inches(*self.plot_size)
        fig.tight_layout()
        sns.despine(fig)
        return fig, ax

    def plot_sensitivity_trajectory(self, normalized=True):
        """Plot the (normalized) sensitivity over time."""
        if normalized:
            points = self.sy_norm.unstack("time").T
        else:
            points = self.sy.unstack("time").T

        fig, ax = plt.subplots()
        ax.plot(self.t, points, alpha=0.75)

        ax.set_xlim(*self._axis_range(self.t, close="right"))
        ax.set_ylim(*self._axis_range(points))
        ax.set_xlabel("Time (s)")
        ax.set_ylabel("Sensitivity")

        # ax.set_title(self.experiment)
        fig.set_size_inches(*self.plot_size)
        fig.tight_layout()
        sns.despine(fig)
        return fig, ax

    def plot_FIM(self):
        """Plot the Fisher Information Matrix as a heatmap."""
        fig = plt.figure()
        v = np.nanmax(np.abs(self.FIM))
        ax = sns.heatmap(
            self.FIM,
            center=0,
            vmin=-v,
            vmax=v,
            cmap=self.divergent_cmap,
            square=True,
            xticklabels=True,
            yticklabels=True,
        )
        fig.set_size_inches(*self.square_plot_size)
        # fig.suptitle(self.experiment, y=1.05)
        fig.tight_layout()
        return fig, ax

    def plot_sensitivity_correlation(
        self, normalized=True, observables=True, cluster=True
    ):
        """Plot the correlations of the (normalized) sensitivity."""
        if observables:
            points = self.sy_norm_corr if normalized else self.sy_corr
        else:
            points = self.sx_norm_corr if normalized else self.sx_corr
        if cluster:
            grid = sns.clustermap(
                points,
                center=0,
                vmin=-1,
                vmax=1,
                cmap=self.divergent_cmap,
                xticklabels=True,
                yticklabels=True,
            )
            return grid.fig, grid
        else:
            fig, ax = plt.subplots()
            sns.heatmap(
                points,
                center=0,
                vmin=-1,
                vmax=1,
                cmap=self.divergent_cmap,
                xticklabels=True,
                yticklabels=True,
                ax=ax,
            )
            fig.set_size_inches(*self.square_plot_size)
            return fig, ax

    def plot_unidentifiable(self, correlation_threshold, normalized=True):
        """Plot the correlaton of the (normalized) sensitivity for highly correlated parameters."""
        points = self.sy_norm_corr if normalized else self.sy_corr
        unidentifiable = (
            points[
                (points.abs() > correlation_threshold)
                & np.triu(np.ones(len(points), bool), k=1)
                # Doesn't presevere the dataframe so loc goes haywire
                # (np.triu(points.abs(), k=1) > correlation_threshold)
            ]
            .dropna(axis=0, how="all")
            .dropna(axis=1, how="all")
        )
        grid = sns.clustermap(
            unidentifiable.fillna(0),
            center=0,
            cmap=self.divergent_cmap,
            vmin=-1,
            vmax=1,
            xticklabels=True,
            yticklabels=True,
            mask=unidentifiable.isna(),
        )
        return grid, grid.fig, unidentifiable.stack()

    def plot_unidentifiable_graph(
        self,
        correlation_threshold,
        normalized=True,
        layout=nx.spring_layout,
        layout_kwargs=None,
    ):
        """Plot the correlaton network of the (normalized) sensitivity for highly correlated parameters."""
        points = self.sy_norm_corr if normalized else self.sy_corr
        unidentifiable = points[
            (points.abs() > correlation_threshold)
            & np.triu(np.ones(len(points), bool), k=1)
            # Doesn't presevere the dataframe so loc goes haywire
            # (np.triu(points.abs(), k=1) > correlation_threshold)
        ].stack()
        G = nx.from_pandas_edgelist(
            unidentifiable.rename_axis(index=["source", "target"]).reset_index(
                name="correlation"
            ),
            edge_attr=["correlation"],
        )

        # Color edges by correlation strength
        edge_color = np.array(list(nx.get_edge_attributes(G, "correlation").values()))
        # Color nodes by number of correlated parameters
        node_color = np.array([G.degree(i) for i in G.nodes])

        if layout_kwargs is None:
            layout_kwargs = {}
        pos = layout(G, **layout_kwargs)

        fig, ax = plt.subplots()
        fig.set_size_inches(*self.square_plot_size)
        nx.draw_networkx_edges(
            G,
            pos,
            edge_color=edge_color,
            edge_cmap=sns.color_palette(self.divergent_cmap, as_cmap=True),
            edge_vmin=-1,
            edge_vmax=1,
            ax=ax,
        )
        nx.draw_networkx_nodes(
            G,
            pos,
            node_size=100,
            node_color=node_color,
            cmap=sns.color_palette(self.sequential_cmap, as_cmap=True),
            ax=ax,
        )
        nx.draw_networkx_labels(G, pos, font_size=8)

        plt.axis("off")
        fig.tight_layout()
        return fig, ax, G

    def plot_pairwise_sensitivity(self, x, y, normalized=True, drawzero=True):
        """Plot the pairwise senitivity of two parameters, seperating the contributions of different observables."""
        points = self.sy_norm if normalized else self.sy
        points = points.loc[:, [x, y], :].unstack("parameter").droplevel(0, 1)

        traces = points.index.get_level_values("observable")
        # colors = traces.map(dict(zip(traces.unique(), self.linecolors)))
        colors = traces.map({v: k for k, v in enumerate(traces.unique())})

        fig, ax = plt.subplots()
        if drawzero:
            ax.axhline(0, linestyle="--", color="#c8c8c8", alpha=0.5)
        if drawzero:
            ax.axvline(0, linestyle="--", color="#c8c8c8", alpha=0.5)

        scatter = ax.scatter(
            points[x],
            points[y],
            c=colors,
            # s=1,
            # cmap=sns.color_palette(self.qualitative_cmap, as_cmap=True)
        )

        label = "Normalized sensitivity ({})" if normalized else "Sensitivity ({})"
        ax.set_xlabel(label.format(x))
        ax.set_ylabel(label.format(y))

        handles, _ = scatter.legend_elements()
        ax.legend(handles, traces.unique(), title="Observables", frameon=False)
        fig.set_size_inches(*self.square_plot_size)
        sns.despine(fig)
        fig.tight_layout()
        return fig, ax

    @staticmethod
    def _axis_range(values, delta=0.01, close=None):
        """Generate a good ax limit with some extra spacing based on the plotted values."""
        mi, almost_mi, almost_ma, ma = np.nanquantile(values, [0, delta, 1 - delta, 1])
        mi_prime = mi - (almost_mi - mi)
        ma_prime = ma + (ma - almost_ma)

        if close == "both":
            return mi, ma
        elif close in ("right", "up", "top"):
            return mi_prime, ma
        elif close in ("left", "down", "bottom"):
            return mi, ma_prime
        else:
            return mi_prime, ma_prime

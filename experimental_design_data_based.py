from experimental_design import *


if __name__ == "__main__":
    # General settings
    recompile = True
    n_samples = 250

    random_seed_value = 5_11_2021

    # Sampling settings
    # Number of parallel processes.
    n_parallel = 4
    # Maximum number of experiments that can fail before simulating the sample in
    # different experiments is stopped. Can be set to None to never stop early.
    maxfail = 1
    # Maximum time a parameter sample can take before the sample simulation
    # is stopped, can be set to None to never stop early.
    # Note: this is for per experiments, but all experiments for a parameter
    #       sample are timed together (n * timeout)
    sample_timeout = 1 * 60
    # TODO: Could also use setMaxTime? Or allow both: per sim / per sample.
    # Does not seem to actually do anything?
    simulation_timeout = 60

    solver_settings = {
        "atol": 1e-12,
        "rtol": 1e-12,
        "sens": "forward",
        "order": "first",
        "nonnegative": False,
    }
    working_directory = pathlib.Path("results_run_data")

    # Set-up working directory & cache
    working_directory.mkdir(exist_ok=True)
    original_model_description_path = pathlib.Path("data/model.xlsx")
    model_description_path = working_directory / "model.xlsx"
    shutil.copy(original_model_description_path, model_description_path)

    # Save some settigns to identify the run later.
    settings = {
        "random_seed": random_seed_value,
        "n_samples": n_samples,
        "n_parallel": n_parallel,
        "maxfail": maxfail,
        "sample_timeout": sample_timeout,
        "simulation_timeout": simulation_timeout,
        **solver_settings,
    }
    settings_path = pathlib.Path(working_directory / "settings.json")
    settings_path.write_text(json.dumps(settings))

    # Build model based on active genes, reactions and other settings
    putida_native_genes = {"fcs", "ech", "curA", "vdh"}
    knockouts = {"ech", "curA", "vdh"}
    knockins = {"CURS1", "DCS", "COMT", "CCoAOMT"}

    active_genes = (putida_native_genes - knockouts) | knockins
    active_reactions = set()
    # Ignore all cinnamic acid reactions for now.
    ignore_reactions = {
        "FCL_CIA",
        "COAHY_CIA",
        "DCS_CICOA",
        "CURS_DCIM",
        "CURS_CIFEM1",
        "CURS_CIFEM2",
        "CURS_CICUM1",
        "CURS_CICUM2",
    }
    # Fix all co-factors to a constant concentration.
    fixed = {
        "AMP",
        "ATP",
        "HCO3",
        "COA",
        "MACOA",
        "PPI",
    }
    if {"CCoAOMT", "COMT"} & knockins:
        fixed |= {"AHCYS", "AMET"}
    if "C3H" in knockins:
        fixed |= {"FAD", "FADH", "O2"}
    implicit = {"H2O", "H"}

    priors = {
        # Base quantities
        Parameters.mu: (-880.0, 680.00),
        # Bit decreased, since it's secondary metabolism.
        Parameters.kv: (1.0, 3),
        Parameters.km: (0.1, 3),
        Parameters.c: (0.1, 3),
        Parameters.u: (0.0001, 3),
        Parameters.ki: (0.1, 3),
        Parameters.ka: (0.1, 3),
    }
    pathway = Pathway(
        pathway_information_path=model_description_path,
        active={"genes": active_genes, "reactions": active_reactions},
        ignore={"reactions": ignore_reactions},
        priors=priors,
        simplifications={
            "enzyme_rate": True,
            # Fix parameters as constants
            # "constants": {"R": None, "T": None},
            # States that should not be added to the formula, except for the calculation of delta mu.
            "implicit_states": implicit,
            # Fix states as constant
            "fixed_states": fixed,
            # Fix reactions as constant (or other expression)
            "fixed_reactions": dict(),
        },
    )

    sbml_model = pathway.symbolic_model.to_sbml_model()
    sbml_model.initial_concentrations = {i: 1 for i in sbml_model.metabolites}
    sbml_model.parameter_values = {i: 1 for i in sbml_model.parameters}

    model_location = working_directory / "model.sbml"
    sbml_model.save(model_location)

    # TODO: The balancing method should be updated to reflect the catalysis of multiple
    #       reactions by a single enzmye. This results in the enzyme concentration being
    #       shared, while each of the reactions has a seperate velocity constant. Binding
    #       constants are partially shared in the case of common substrates. How does this
    #       work with the Q matrix?
    #       Anyway, for now we simply ignore this when taking samples.
    parameter_location = working_directory / "parameters.npz"
    pathway.balanced_parameters.save(parameter_location)
    pathway._used_parameter_data.df.to_csv(
        working_directory / "input_parameters.debug.csv"
    )
    pathway.balanced_parameters.to_frame(mean=True).T.to_csv(
        working_directory / "balanced_parameters.debug.csv"
    )

    fixed_parameters = ["R", "T"]

    # Define and save experimental designs
    experimental_data_file = pathlib.Path("data/measurement_oct_12_cleaned.xlsx")
    with pd.ExcelFile(experimental_data_file) as file:
        strains = file.parse(sheet_name="strains", header=0).set_index(
            ["strain", "location"]
        )
        experiments = file.parse(sheet_name="experiments", header=0).set_index(
            "experiment"
        )
        measurements = file.parse(sheet_name="measurements", header=0).set_index(
            ["experiment", "strain"]
        )

    # We ignore the experiments with TAL/C3H or glc/tyr as substrates,
    # as well as the experiments not using CURS1.
    measurements = measurements.loc[
        ~(
            measurements.index.get_level_values("experiment").str.startswith("Glc")
            | measurements.index.get_level_values("experiment").str.startswith("Tyr")
            | measurements.index.get_level_values("strain").str.contains("TAL")
            | measurements.index.get_level_values("strain").str.contains("C3H")
        )
        & measurements.index.get_level_values("strain").str.contains("CURS1")
    ]
    experiments = experiments.drop("TYR", axis=1)

    design = []
    for i, (experiment, strain) in enumerate(measurements.index.unique()):
        design.append((i, "name", "experiment", experiment))
        design.append((i, "name", "strain", strain))
        for metabolite, concentration in experiments.loc[experiment].items():
            design.append((i, "initial", metabolite, concentration))
        if strains.loc[strain].COMT.any():
            design.append((i, "parameter", "u_v__COMT__COMT", None))
        else:
            design.append((i, "parameter", "u_v__COMT__COMT", 0))
        if strains.loc[strain].CCOAOMT.any():
            design.append((i, "parameter", "u_v__CCOAOMT__CCOAOMT", None))
        else:
            design.append((i, "parameter", "u_v__CCOAOMT__CCOAOMT", 0))

    design = (
        pd.DataFrame(design, columns=["experiment", "type", "identifier", "value"])
        .pivot(index="experiment", columns=["type", "identifier"])
        .droplevel(axis=1, level=0)
    )
    design.to_csv(working_directory / "experimental_design.csv")

    # Define and save measurements
    simulation_time = 250
    timepoints = np.linspace(0, simulation_time, 101)
    measured = ["CAA", "CUA", "FEA", "CUR", "DCUR", "BDCUR"]
    measurements = (
        pd.DataFrame(1, columns=measured, index=timepoints)
        .astype(bool)
        .rename_axis("timepoints")
    )
    measurements.to_csv(working_directory / "measurements.csv")

    # Compile model
    # TODO: This should be cached
    amici_directory = working_directory / "amici"
    model_name = "model"
    if recompile:
        module_directory = compile(
            model_location, model_name, amici_directory, measurements, fixed_parameters
        )
    else:
        module_directory = amici_directory / model_name

    # Sample parameter set
    reaction_enzyme_mapping_placeholder = {
        "4CL": ("FCL_CUA", "FCL_FEA", "FCL_CAA"),
        "CURS": ("CURS_CUR", "CURS_BDCUR", "CURS_DCUR1", "CURS_DCUR2"),
        "DCS": ("DCS_CUCOA", "DCS_FECOA"),
    }
    symbolic_model = symbolicSBML.SBMLModel(model_location)
    original_parameters = pd.Series(symbolic_model.parameter_values)

    with random_seed(random_seed_value):
        samples = sample_parameters(
            pathway.balanced_parameters,
            [i for i in symbolic_model.parameters if i not in fixed_parameters],
            n_samples,
            reaction_enzyme_mapping_placeholder,
        )

    parameter_sets = samples
    initial_concentrations = pd.Series(symbolic_model.initial_concentrations)
    initial_concentrations = (
        pd.concat([initial_concentrations] * n_samples, axis=1)
        .rename_axis(index="metabolite", columns="set")
        .T
    )
    initial_concentrations.loc[:, ["CAA", "CUA", "FEA"]] = 0
    initial_concentrations.loc[:, ["CACOA", "CUCOA", "FECOA"]] = 0
    initial_concentrations.loc[:, ["DCUCOA", "DFECOA"]] = 0
    initial_concentrations.loc[:, ["CUR", "DCUR", "BDCUR"]] = 0

    parameter_sets.to_csv(working_directory / "parameters.csv")

    # TODO: Could be indididual per experiment as well
    # TODO: Take more timepoints and use them to select
    # an optimal subset that is achievable experimentally.
    timepoints = measurements.index.to_numpy()

    simulations = []
    n_samples = len(parameter_sets)
    # Create the set of parameters and initial concentrations per experiment.
    for i in design.index:
        # Add relevant fixed parameter values for the experiment to the total parameter set.
        parameter_df = parameter_sets.assign(
            **{
                # TODO: Fix this hardcoded override.
                "R": pathway.balanced_parameters.R,
                "T": pathway.balanced_parameters.T,
            }
        )
        # Update parameters (if not None/NaN)
        update_parameter_keys = [
            k for k, v in design.loc[i, "parameter"].items() if not pd.isna(v)
        ]
        parameter_df.loc[:, update_parameter_keys] = design.loc[
            [i] * n_samples, pd.IndexSlice["parameter", update_parameter_keys]
        ].values

        # Update initial concentrations
        update_concentration_keys = [
            k for k, v in design.loc[i, "initial"].items() if not pd.isna(v)
        ]
        initial_concentration_df = initial_concentrations.copy()
        initial_concentration_df.loc[:, update_concentration_keys] = design.loc[
            [i] * n_samples, pd.IndexSlice["initial", update_concentration_keys]
        ].values

        for j in range(n_samples):
            ic = initial_concentration_df.loc[j]
            p = parameter_df.loc[j]
            simulations.append((i, j, ic, p))

    output_directory = working_directory / "simulations"
    output_directory.mkdir(exist_ok=False)
    print("Simulating data")
    run_simulations(
        model_name=model_name,
        module_directory=module_directory,
        timepoints=timepoints,
        simulations=simulations,
        output_directory=output_directory,
        n_parallel=n_parallel,
        solver_settings=solver_settings,
        max_fail=maxfail,
        simulation_timeout=simulation_timeout,
        sample_timeout=sample_timeout,
        return_data=False,
    )

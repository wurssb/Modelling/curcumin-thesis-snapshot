import pandas as pd
from rdkit import Chem
from rdkit.Chem import AllChem, Draw, rdAbbreviations
import pathlib

results_directory = pathlib.Path("results/molecules")
results_directory.mkdir(exist_ok=True, parents=True)


model_metabolites = {
    "TYR",
    "PHE",
    "LDOPA",
    "CUA",
    "CAA",
    "FEA",
    "CIA",
    "CUCOA",
    "CACOA",
    "FECOA",
    "CICOA",
    "DCUCOA",
    "DFECOA",
    "DCICOA",
    "CUR",
    "BDCUR",
    "DCIM",
    "DCUR",
    "CIFEM",
    "CICUM",
    "COA",
}

df = pd.read_excel("data/model.xlsx", sheet_name="Metabolites")
df = df.loc[df.ID.isin(model_metabolites)]

molecules = {}
for name, inchi in df[["ID", "InChi"]].dropna().itertuples(index=False):
    molecule = Chem.MolFromInchi(inchi)
    molecule.Compute2DCoords()
    molecules[name] = molecule

# Create abbrevation for -CoA and -O-CH3
coa = molecules.pop("COA")
sulfur = [idx for idx, atom in enumerate(coa.GetAtoms()) if atom.GetAtomicNum() == 16]
assert len(sulfur) == 1
smiles = Chem.MolToSmiles(coa, rootedAtAtom=sulfur[0])
abbrv = rdAbbreviations.ParseAbbreviations("MeO OC\nCoA {}".format(smiles))
abbrv[0].displayLabel = "OMe"
abbrv[0].displayLabelW = "MeO"
abbrv[1].displayLabel = "CoA"
abbrv[1].displayLabelW = "CoA"

# Drawing options
draw_options = Draw.MolDrawOptions()
draw_options.useBWAtomPalette()
draw_options.rotate = 163.6  # Based on curcumin being straight.
draw_options.fixedBondLength = 30
draw_options.fixedBondLength = 30

# Phenylpropane as template so all molecules align the same.
template_1 = Chem.MolFromSmiles("CCCc1ccccc1")
template_1.Compute2DCoords()

# Phenylpropene as second template, for molecules with the double bond there.
template_2 = Chem.MolFromSmiles("C1=CC=CC(/C=C/C)=C1")
template_2.Compute2DCoords()

# Allign the templates to each other based on the Phe-C
AllChem.GenerateDepictionMatching2DStructure(
    template_2, template_1, refPatt=Chem.MolFromSmiles("Cc1ccccc1")
)


formats = ("svg", "png")
for name, molecule in molecules.items():
    molecule.Compute2DCoords()

    molecule = rdAbbreviations.CondenseMolAbbreviations(
        molecule, abbrv, maxCoverage=1.0
    )

    try:
        AllChem.GenerateDepictionMatching2DStructure(molecule, template_1)
    except ValueError:
        try:
            AllChem.GenerateDepictionMatching2DStructure(molecule, template_2)
        except:
            print("Could not match {} to templates".format(name))

    for f in formats:
        Draw.MolToFile(
            molecule,
            results_directory / "{}.{}".format(name, f),
            imageType=f,
            size=(2500, 2500),
            wedgeBonds=False,
            options=draw_options,
        )

# Modify svgs to use rounded line-cap style.
for path in (path for path in results_directory.iterdir() if path.suffix == ".svg"):
    path.write_text(
        path.read_text().replace("stroke-linecap:butt", "stroke-linecap:round")
    )

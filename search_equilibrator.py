import pathlib

import equilibrator_api
import pandas as pd
from pint.compat import eq


def predict(cc, hit):
    return cc.predictor.standard_dgf_prime(
        hit, cc.p_h, cc.ionic_strength, cc.temperature, cc.p_mg
    )


if __name__ == "__main__":
    path = pathlib.Path("data/model.xlsx")
    metabolites = (
        pd.read_excel(path, sheet_name="Metabolites")
        .dropna(axis=1, how="all")
        .dropna(axis=0, how="all")
    )

    cc = equilibrator_api.ComponentContribution()
    Q = equilibrator_api.Q_

    # Set conditions. Note that these should be kept in correspondence with the numbers used
    # during balancing!
    cc.p_h = Q(7.5)
    cc.ionic_strength = Q(0.25, "M")
    cc.p_mg = Q(3.0)
    cc.temperature = Q(300, "K")

    dg = {}
    for i, row in metabolites.iterrows():
        print(row.Name, row.InChiKey)
        hits = cc.search_compound_by_inchi_key(row.InChiKey)
        if len(hits) == 0:
            # Since not all different charge options are indexed by eQuilibrator,
            # we can vary the last letter of the InChiKey denoting the charge.
            # Valid options are:
            # N (0), A (> 12, < -12)
            # -1 | MLKJIHGFEDCB | -12
            # +1 | OPQRSTUVWXYZ | + 12
            for letter in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                new_key = row.InChiKey[:-1] + letter
                hits.extend(cc.search_compound_by_inchi_key(new_key))

        for hit in hits:
            sdfgp = predict(cc, hit)
            print("    {:<7}".format(hit.id), sdfgp)
        print()
        # Sort by ID, it seems like the lower ones are usually more correct.
        # Also what equilibrator does when you search per name.
        sorted(hits, key=lambda x: x.id)

        if hits:
            hit = hits[0]
            dg[row.ID] = (predict(cc, hit), hit, "")
        else:
            dg[row.ID] = None

    # Some compounds cannot be found. Make estimates
    CUA = dg["CUA"][1]
    CAA = dg["CAA"][1]
    FEA = dg["FEA"][1]
    CIA = dg["CIA"][1]
    CUCOA = dg["CUCOA"][1]
    CACOA = dg["CACOA"][1]
    FECOA = dg["FECOA"][1]
    CICOA = dg["CICOA"][1]
    DCUCOA = dg["DCUCOA"][1]
    DFECOA = dg["DFECOA"][1]
    CUR = dg["CUR"][1]
    BDCUR = dg["BDCUR"][1]
    DCUR = dg["DCUR"][1]
    CICUM = dg["CICUM"][1]

    # DCICOA
    # Assume the difference between CICOA and DCICOA is similar to
    # difference between the other diketides and their precursors.
    assert dg["DCICOA"] is None
    # Calculate the value for DCICOA manually.
    # Error is overestimated since some terms should cancel out, but the information is
    # not saved after extraction from equilibrator.
    # delta = [
    #     DCUCOA - CUCOA,
    #     DFECOA - FECOA,
    # ]
    # dg["DCICOA"] = (CICOA - sum(delta) / len(delta), None)

    # Instead we use the equilibrator reaction to calculate the error properly.
    dg["DCICOA"] = DCICOA = (
        cc.standard_dg_prime(
            equilibrator_api.Reaction(
                {
                    CICOA: -1,
                    DCUCOA: 0.5,
                    CUCOA: -0.5,
                    DFECOA: 0.5,
                    FECOA: -0.5,
                }
            )
        ),
        None,
        "Inferred from precursors and similar compounds",
    )
    assert dg["CIFEM"] is None
    assert dg["DCIM"] is None
    # Same for DCIM and CIFEM, should be similar to precursor + diketide.
    # However, do not include the DCICOA dependent metabolites since it was already estimated itself.
    # delta = [
    #     CUR - (FECOA + DFECOA),
    #     BDCUR - (CUCOA + DCUCOA),
    #     DCUR - (FECOA + DCUCOA),
    #     DCUR - (CUCOA + DFECOA),
    #     CICUM - (CICOA + DCUCOA),
    # ]
    # dg["CIFEM"] = (DFECOA + CICOA + sum(delta) / len(delta), None)
    # dg["DCIM"] = (DCICOA + CICOA + sum(delta) / len(delta), None)

    dg["CIFEM"] = (
        cc.standard_dg_prime(
            equilibrator_api.Reaction(
                {
                    DFECOA: 1 - 0.2 - 0.2,
                    CICOA: 1 - 0.2,
                    CUR: 0.2,
                    FECOA: -0.2 - 0.2,
                    BDCUR: 0.2,
                    CUCOA: -0.2 - 0.2,
                    DCUCOA: -0.2 - 0.2 - 0.2,
                    DCUR: 0.2 + 0.2,
                    CICUM: 0.2,
                }
            )
        ),
        None,
        "Inferred from precursors and similar compounds",
    )

    dg["DCIM"] = (
        cc.standard_dg_prime(
            equilibrator_api.Reaction(
                {
                    DFECOA: -0.5 - 0.2 - 0.2,
                    CICOA: +1 + 1 - 0.2,
                    CUR: 0.2,
                    FECOA: 0.5 - 0.2 - 0.2,
                    BDCUR: 0.2,
                    CUCOA: 0.5 - 0.2 - 0.2,
                    DCUCOA: -0.5 - 0.2 - 0.2 - 0.2,
                    DCUR: 0.2 + 0.2,
                    CICUM: 0.2,
                }
            )
        ),
        None,
        "Inferred from precursors and similar compounds",
    )

    assert all(i[0] is not None for i in dg.values())

    # The errors for H and AMET are incorrect.
    # H+ (mu=0) has error of 1e5 (equilibrator_api.default_rmse_inf),
    # maybe went wrong with log-transform? I think H+ is supposed
    # to be the reference point, so it should also be defined as 0 error?
    # To avoid this same error later we just fix it at 0.01 instead.
    assert dg["H"][0].m.s >= 1e3
    dg["H"] = (
        equilibrator_api.ureg.Measurement(
            dg["H"][0].m.n,
            0.01,
            dg["H"][0].units,
        ),
        dg["H"][1],
        "Modified error",
    )

    # Assume it should be 5.06 instead of 100,005.6
    assert dg["AMET"][0].m.s > 1e3
    dg["AMET"] = (
        equilibrator_api.ureg.Measurement(
            dg["AMET"][0].m.n,
            dg["AMET"][0].m.s - equilibrator_api.default_rmse_inf.m,
            dg["AMET"][0].units,
        ),
        dg["AMET"][1],
        "Modified error",
    )

    for key, value in dg.items():
        if value is not None:
            if value[1] is not None:
                print(
                    ",".join(
                        (
                            key,
                            str(value[1].id),
                            value[1].inchi_key,
                            str(value[0].m.n),
                            str(value[0].m.s),
                            value[2],
                        )
                    )
                )
            else:
                print(
                    ",".join(
                        (key, "", "", str(value[0].m.n), str(value[0].m.s), value[2])
                    )
                )
        else:
            print(key)

# Alternative way, per equilibrator documentation that also generates the covariance.
# Does not work witht he missing compounds.
import numpy as np

compounds = {k: v[1] for k, v in dg.items() if v[1] is not None and k != "H"}
compound_names = list(compounds.keys())
compounds = list(compounds.values())

standard_dgf_mu, sigmas_fin, sigmas_inf = zip(*map(cc.standard_dg_formation, compounds))
standard_dgf_mu = np.array(standard_dgf_mu)
sigmas_fin = np.array(sigmas_fin)
sigmas_inf = np.array(sigmas_inf)

# we now apply the Legendre transform to convert from the standard ΔGf to the standard ΔG'f
delta_dgf_list = np.array(
    [
        cpd.transform(cc.p_h, cc.ionic_strength, cc.temperature, cc.p_mg).m_as("kJ/mol")
        for cpd in compounds
    ]
)
standard_dgf_prime_mu = pd.Series(
    standard_dgf_mu + delta_dgf_list, index=compound_names
)

# AHMET has a significantly higher value for sigma_inf[24] then *anything* else.
# e.g 1 vs (-1.8 +- 3.09)e-16.
# Perhaps 1 is a defualt value? We replace it with 0 instead...
assert sigmas_inf[compound_names.index("AMET"), 24] > 1
sigmas_inf[compound_names.index("AMET"), 24] = 0

# to create the formation energy covariance matrix, we need to combine the two outputs
# sigma_fin and sigma_inf
standard_dgf_cov = pd.DataFrame(
    sigmas_fin @ sigmas_fin.T + 1e6 * sigmas_inf @ sigmas_inf.T,
    index=compound_names,
    columns=compound_names,
)

with pd.ExcelWriter("data/mu.xlsx") as writer:
    standard_dgf_prime_mu.to_excel(writer, sheet_name="mu_mean")
    standard_dgf_cov.to_excel(writer, sheet_name="mu_cov")
    pd.Series(
        {
            "p_h": cc.p_h.m,
            "ionic_strength (M)": cc.ionic_strength.m,
            "p_mg": cc.p_mg.m,
            "temperature (K)": cc.temperature.m,
        },
        name="conditions",
    ).to_excel(writer, sheet_name="conditions")

# import matplotlib.pyplot as plt
# import seaborn as sns

# # Make the figure with a subset of the compounds
# selection = [
#     "BDCUR",
#     "CAA",
#     "CACOA",
#     "CUA",
#     "CUCOA",
#     "CUR",
#     "DCUCOA",
#     "DCUR",
#     "DFECOA",
#     "FEA",
#     "FECOA",
#     "MACOA",
#     "PPI",
#     "AMET",
#     "COA",
#     "AMP",
#     "CO2",
#     "AHCYS",
#     "ATP",
# ]
# grid = sns.clustermap(
#     standard_dgf_cov.loc[selection, selection],
#     cmap="RdBu",
#     center=0,
#     # ax=ax,
#     # norm=matplotlib.colors.LogNorm(),
#     xticklabels=True,
#     yticklabels=True,
# )
# grid.figure.set_size_inches(15, 12)
# # grid.tight_layout()
# grid.savefig("results/mu_cov.png", dpi=300)

# df = (
#     standard_dgf_prime_mu.loc[selection]
#     .to_frame()
#     .set_axis(["Mean"], axis=1)
#     .assign(Std=np.sqrt(np.diag(standard_dgf_cov.loc[selection, selection])))
#     .iloc[grid.dendrogram_col.reordered_ind]
# )

# fig, ax = plt.subplots()
# df.Mean.plot(kind="bar", yerr=df.Std, ax=ax)
# ax.set_xlabel("Compound")
# ax.set_ylabel("Mu (kJ/mol)")
# sns.despine(fig)
# fig.tight_layout()
# fig.set_size_inches(7, 5)
# fig.savefig("results/mu.png", dpi=300)

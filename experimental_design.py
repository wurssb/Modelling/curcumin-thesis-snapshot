import collections
import concurrent.futures
import contextlib
import enum
import importlib
import io
import itertools
import json
import logging
import os
import pathlib
import re
import shutil
import sys
import time

import amici
import libsbml
import numpy as np
import pandas as pd
import pebble
import pyDOE2 as pyDOE
import symbolicSBML
from symbolicSBML import Parameters

from AmiciResultWrapper import AmiciResultWrapper
from pathway import Pathway


@enum.unique
class SimulationStatus(enum.Enum):
    """Enumeration for sample simulation status."""

    success = 1
    timeout = 2
    maxfail = 3


amici_skip_status = {"AMICI_PREEQUILIBRATE"}
AmiciStatus = enum.unique(
    enum.Enum(
        "AmiciStatus",
        {
            status: amici.__dict__[status]
            for status in dir(amici)
            if status.startswith("AMICI_")
            and status.isupper()
            and status not in amici_skip_status
            and amici.__dict__[status] <= 0
        },
    )
)


@contextlib.contextmanager
def random_seed(seed):
    """Context manager to get consistent random values for numpy.

    Preserves and resets the state of current generator at the end of the context."""

    state = np.random.get_state()
    np.random.seed(seed)
    try:
        yield
    finally:
        np.random.set_state(state)


def create_design(factors):
    levels = [len(d["levels"]) for d in factors for i in d["keys"]]
    columns = [(d["name"], i) for d in factors for i in d["keys"]]

    df = (
        pd.DataFrame(
            pyDOE.fullfact(levels),
            columns=pd.MultiIndex.from_tuples(columns, names=["name", "keys"]),
        )
        .rename_axis("experiment")
        .astype(int)
    )
    # Replace with actual values
    for d in factors:
        df.loc[:, pd.IndexSlice[d["name"], :]] = df.loc[
            :, pd.IndexSlice[d["name"], :]
        ].replace(dict(enumerate(d["levels"])))
    return df


def update_model_with_events(model_location, new_location, events):
    doc = libsbml.readSBMLFromFile(str(model_location))
    sbml_model = doc.getModel()
    event_parameters = []
    for name, event in events.items():
        for parameter in event["parameters"]:
            sbml_parameter = sbml_model.createParameter()
            sbml_parameter.setId(parameter)
            sbml_parameter.setConstant(True)
            sbml_parameter.setValue(0.0)
            event_parameters.append(parameter)

        sbml_event = sbml_model.createEvent()
        sbml_event.setId(name)
        sbml_event.setName(name)
        sbml_event.setUseValuesFromTriggerTime(True)

        sbml_trigger = sbml_event.createTrigger()
        sbml_trigger.setMath(libsbml.parseL3Formula(event["trigger"]))
        sbml_trigger.setPersistent(True)
        sbml_trigger.setInitialValue(True)

        if len(event["targets"]) != len(event["assignments"]):
            raise ValueError(
                "Mismatch between number of targets and assignments for: {}".format(
                    name
                )
            )

        for target, assignment in zip(event["targets"], event["assignments"]):
            sbml_assignment = sbml_event.createEventAssignment()
            sbml_assignment.setVariable(target)
            sbml_assignment.setMath(libsbml.parseL3Formula(assignment))
    libsbml.writeSBMLToFile(
        doc,
        str(new_location),
    )
    return event_parameters


def compile(
    sbml_file, model_name, model_directory, measurements, fixed_parameters=None
):
    sbml_importer = amici.SbmlImporter(sbml_file)
    sbml_importer.sbml2amici(
        model_name,
        model_directory,
        verbose=logging.INFO,
        # Add an observable for each metabolite that is measured at least once.
        observables={
            "o_{}".format(i): {"name": i, "formula": i}
            for i in measurements.columns[measurements.any(axis=0)]
        },
        # Constant parameters will not have sensitivities calculated.
        constant_parameters=fixed_parameters,
        sigmas={},
    )
    return model_directory / model_name


def setup_solver(
    model_directory,
    model_name,
    atol=1e-8,
    rtol=1e-8,
    sens="forward",
    order="first",
    nonnegative=False,
):
    sys.path.insert(0, os.path.abspath(model_directory))
    try:
        model_module = importlib.import_module(model_name)
    finally:
        sys.path.pop(0)
    model = model_module.getModel()

    solver = model.getSolver()
    solver.setRelativeTolerance(rtol)
    solver.setAbsoluteTolerance(atol)

    model.requireSensitivitiesForAllParameters()

    scaling = amici.parameterScalingFromIntVector(
        [
            amici.ParameterScaling_log10
            if symbolicSBML.Parameters.split(i)[0] in symbolicSBML.Parameters.log
            else amici.ParameterScaling_none
            for i in model.getParameterNames()
        ]
    )
    model.setParameterScale(scaling)

    if nonnegative:
        model.setAllStatesNonNegative()

    if sens == "forward":
        solver.setSensitivityMethod(amici.SensitivityMethod.forward)
    elif sens == "adjoint":
        solver.setSensitivityMethod(amici.SensitivityMethod.adjoint)

    if order == "first":
        solver.setSensitivityOrder(amici.SensitivityOrder.first)
    elif order == "second":
        solver.setSensitivityOrder(amici.SensitivityOrder.second)

    return model, solver


def run_experiment(model, solver, initial_concentrations, parameters):
    state_order = np.array(model.getStateNames())
    if diff := set(initial_concentrations.index) ^ set(state_order):
        raise ValueError("Excess/missing initial states provided: {}".format(diff))
    model.setInitialStates(initial_concentrations.loc[state_order])

    parameter_order = np.array(model.getParameterNames())
    fixed_parameter_order = np.array(model.getFixedParameterNames())
    if diff := set(parameters.index) ^ (
        set(parameter_order) | set(fixed_parameter_order)
    ):
        raise ValueError("Excess/missing (fixed) parameters provided: {}".format(diff))
    model.setParameters(parameters.loc[parameter_order])
    model.setFixedParameters(parameters.loc[fixed_parameter_order])
    return amici.runAmiciSimulation(model, solver)


def FIM_measures(FIM, criterion, nan_behaviour="pass"):
    if np.isnan(FIM).any():
        if nan_behaviour == "pass":
            return np.NaN
        elif nan_behaviour == "raise":
            raise ValueError("NaN value in FIM.")
        else:
            raise ValueError(
                "Invalid setting for nan_behaviour: {}.".format(nan_behaviour)
            )

    # A-optimality: Maximize trace(FIM)
    # Minimizes the sum of the variances of the parameter estimates.
    if criterion == "A":
        return np.trace(FIM)

    # TODO: Which is the true A criterion?
    # A - optimality: Minimize trace(inv(FIM))
    if criterion == "Am":
        return np.trace(np.linalg.inv(FIM))

    # D-optimality: Maximize det(FIM)
    # Minimizes the generalized variance (=volume?) of the parameter estimates.
    if criterion == "D":
        return np.linalg.det(FIM)

    # TODO: Valid to look at real part only?
    # E-optimality: Maximize smallest eigenvalue of FIM.
    # Minimizes the variance of the parameter with the maximum variance.
    if criterion == "E":
        return np.linalg.eigvals(FIM).real.min()
    # TODO: Which is the true E criterion?
    # A - optimality: Minimize largest eigenvalue of inv(FIM)
    if criterion == "Ep":
        return np.linalg.eigvals(np.linalg.inv(FIM)).real.max()

    # Modified-E-optimality: Minimize ratio of max eigenvalue to min eigenvalue.
    # Minimizes the ratio of the longest and shortest axes of the information matrix.
    if criterion == "Em":
        eigvals = np.linalg.eigvals(FIM).real
        return np.abs(eigvals.max() / eigvals.min())


def sample_parameters(
    balanced_parameters, model_parameters, n, reaction_enzyme_mapping_placeholder
):
    # TODO: This should take into account enzymes as well as reactions.

    # Match indices (output of samples is sorted)
    base_order = pd.Index(balanced_parameters.columns).sort_values()
    model_order = pd.Series(model_parameters).apply(Parameters.split, args=("", False))

    # Special cases for merged parameters
    # Bit of a mess here since the balancing results are not properly assigned to enzymes,
    # just reactions. Thus we have the placeholder mapping - note that this is a placeholder only,
    # as the balanced parameters don't necessarily match the true model representation.
    order = []
    special = []
    missing_indicator = -1
    for (ptype, met, reac, enzyme) in model_order:
        if ptype in Parameters.combined:
            terms = []
            order.append(missing_indicator)
            if ptype in (Parameters.u_v):
                for ptype in (Parameters.kv, Parameters.u):
                    loc, level = base_order.get_loc_level((ptype, met, reac))
                    if level != None:
                        raise KeyError(
                            "Couldn't find parameter: {} / {} / {}".format(
                                [ptype, met, reac]
                            )
                        )
                    else:
                        terms.append(loc)
            special.append(terms)
        else:
            special.append(missing_indicator)
            try:
                if (
                    ptype in (Parameters.km, Parameters.u)
                    and enzyme in reaction_enzyme_mapping_placeholder
                ):
                    for placeholder in reaction_enzyme_mapping_placeholder[enzyme]:
                        try:
                            loc, level = base_order.get_loc_level(
                                (ptype, met, placeholder)
                            )
                            break
                        except KeyError:
                            continue

                    else:
                        raise KeyError
                elif ptype == Parameters.kv:
                    loc, level = base_order.get_loc_level((ptype, met, reac))
                else:
                    loc, level = base_order.get_loc_level((ptype, met, reac + enzyme))

                if level != None:
                    raise KeyError
                order.append(loc)
            except KeyError:
                order.append(missing_indicator)
    samples = balanced_parameters.sample(n, only_base=False, noRT=True)
    model_samples = samples.iloc[:, order].set_axis(model_parameters, axis=1)
    for i, locations in enumerate(special):
        if locations == missing_indicator:
            continue
        accumulator = pd.Series(1, index=model_samples.index)
        for location in locations:
            accumulator *= samples.iloc[:, location]
        model_samples.iloc[:, i] = accumulator
    return model_samples


def pool_initiator(model_name, module_directory, timepoints, solver_settings):
    # Use a global variable for the model and solver so we can reuse it per child process.
    global model, solver
    model, solver = setup_solver(module_directory, model_name, **solver_settings)
    model.setTimepoints(timepoints)


def run_simulation_group(
    group, working_directory, max_fail=None, max_time=None, silence_amici=True
):
    # Context to silence errors, or a dummy if it's unwanted.
    if silence_amici:
        context = contextlib.redirect_stdout
    else:
        context = contextlib.nullcontext

    # Does not seem to actually work.
    if max_time:
        solver.setMaxTime(max_time)

    fail_counter = 0
    for experiment, sample, ic, p in group:
        # print("Running: sample {}, experiment {}".format(sample, experiment))
        with context(io.StringIO()):
            # Note that model and solver are per process global variables here.
            t0 = time.time()
            rdata = run_experiment(model, solver, ic, p)
            t = time.time() - t0
        if rdata.status != AmiciStatus.AMICI_SUCCESS.value:
            print(
                "\tFailed: {}/{} with status code: {} after {:.3f}s".format(
                    sample, experiment, AmiciStatus(rdata.status), t
                )
            )
            fail_counter += 1
            if max_fail is not None and fail_counter > max_fail:
                print("\tAborting sample {} early!".format(sample))
                return SimulationStatus.maxfail

        result = AmiciResultWrapper(rdata, model)
        result.to_hdf(
            working_directory / "s{}e{}.h5".format(sample, experiment), compress=True
        )

    return SimulationStatus.success


def create_simulations_from_design(
    parameter_sets,
    initial_concentrations,
    design,
    pathway,
):
    simulations = []
    n_samples = len(parameter_sets)
    # Create the set of parameters and initial concentrations per experiment.
    for i in design.index:
        # Add relevant fixed parameter values for the experiment to the total parameter set.
        parameter_df = parameter_sets.assign(
            **{
                # TODO: Fix this hardcoded override.
                "R": pathway.balanced_parameters.R,
                "T": pathway.balanced_parameters.T,
                "event_t_addition": design.loc[
                    i, pd.IndexSlice["parameter", "event_t_addition"]
                ],
                "event_c_addition_CAA": design.loc[i, pd.IndexSlice["addition", "CAA"]],
                "event_c_addition_CUA": design.loc[i, pd.IndexSlice["addition", "CUA"]],
                "event_c_addition_FEA": design.loc[i, pd.IndexSlice["addition", "FEA"]],
            }
        )
        initial_concentration_df = initial_concentrations.copy()
        initial_concentration_df.loc[:, ["CAA", "CUA", "FEA"]] = design.loc[
            [i] * n_samples, pd.IndexSlice["initial", ["CAA", "CUA", "FEA"]]
        ].values

        for j in range(n_samples):
            ic = initial_concentration_df.loc[j]
            p = parameter_df.loc[j]
            simulations.append((i, j, ic, p))
    return simulations


def run_simulations(
    model_name,
    module_directory,
    timepoints,
    simulations,
    output_directory,
    n_parallel=None,
    solver_settings=None,
    max_fail=None,
    sample_timeout=None,
    simulation_timeout=None,
    return_data=True,
):
    if solver_settings is None:
        solver_settings = {}

    n_experiments = len({s[0] for s in simulations})
    # Group per parameter sample.
    # This way, we can abort this sample if it fails on too many experiments.
    # TODO: Just generate the thing in this format...
    grouped = itertools.groupby(
        sorted(simulations, key=lambda x: (x[1], x[0])), key=lambda x: x[1]
    )

    # Run the simulations in serial or parallel.
    status = {}
    if n_parallel == None:
        pool_initiator(model_name, module_directory, timepoints, solver_settings)
        for sample, group in grouped:
            status[sample] = run_simulation_group(
                list(group), output_directory, max_fail, simulation_timeout
            )

    else:
        futures = []
        with pebble.ProcessPool(
            max_workers=n_parallel,
            initializer=pool_initiator,
            initargs=(model_name, module_directory, timepoints, solver_settings),
        ) as pool:
            for sample, group in grouped:
                futures.append(
                    (
                        sample,
                        pool.schedule(
                            run_simulation_group,
                            args=(
                                list(group),
                                output_directory,
                                max_fail,
                                simulation_timeout,
                            ),
                            timeout=sample_timeout,
                        ),
                    )
                )
            for sample, result in futures:
                try:
                    status[sample] = result.result()
                except concurrent.futures.TimeoutError:
                    print(
                        "Sample {} timed out after {} seconds".format(
                            sample, sample_timeout
                        )
                    )
                    status[sample] = SimulationStatus.timeout

    # Gather all results and concatenate into one large hdf file.
    # Maybe using h5.h5o.copy? https://stackoverflow.com/a/30610511
    # Or h5copy cmd line utility? https://portal.hdfgroup.org/display/HDF5/h5copy

    # # TODO: Can't get this to work withouth error or segfault.
    # with h5.File(working_directory / "results.h5", "w") as results_file:
    #     results_file.create_group("s{}".format(sample))
    #     for sample, experiment, path in sorted(result_files):
    #         h5_path = "s{}/e{}".format(sample, experiment)
    #         results_file.create_group(h5_path)
    #         with h5.File(path, "r") as partial_results_file:
    #             # for i in ["index", "data", "diagnostics"]:
    #             #     partial_results_file.copy(
    #             #         partial_results_file[i],
    #             #         results_file["h5_path" + i],
    #             #     )

    #             h5.h5o.copy(
    #                 partial_results_file.id,
    #                 bytes("/", encoding="utf8"),
    #                 results_file.id,
    #                 bytes(h5_path, encoding="utf8"),
    #             )

    # Remove temporary results directory.
    # Repack to save space?

    # TODO: Save status codes somewhere + time of crash + simulation time for all runs.
    if return_data:
        return load_simulation_data(output_directory, n_experiments, status)
    # (
    #     {(s, e): AmiciResultWrapper.from_hdf(path) for s, e, path in result_files},
    #     {(s, e): AmiciResultWrapper.from_hdf(path) for s, e, path in failures},
    # )
    # return status


def load_simulation_data(directory, n_experiments, status=None, limit_samples=None):
    # TODO: Clean this mess up
    all_results = []
    result_files = []
    failures = []
    for path in directory.iterdir():
        match = re.match(r"s(?P<sample>\d+)e(?P<experiment>\d+).h5", path.name)
        if match is not None:
            sample = int(match.group("sample"))
            if limit_samples is not None and sample >= limit_samples:
                continue
            experiment = int(match.group("experiment"))
            if status is None:
                all_results.append((sample, experiment, path))
            elif status[sample] == SimulationStatus.success:
                result_files.append((sample, experiment, path))
            else:
                failures.append((sample, experiment, path))

    if status is None:
        # Manually check for failures
        count = collections.defaultdict(set)
        for sample, experiment, _ in all_results:
            count[sample].add(experiment)

        complete_set = set(range(n_experiments))
        complete = {
            sample
            for sample, experiments in count.items()
            if experiments == complete_set
        }
        return (
            {
                (s, e): AmiciResultWrapper.from_hdf(path)
                for s, e, path in all_results
                if s in complete
            },
            {
                (s, e): AmiciResultWrapper.from_hdf(path)
                for s, e, path in all_results
                if s not in complete
            },
        )
    else:
        return (
            {(s, e): AmiciResultWrapper.from_hdf(path) for s, e, path in result_files},
            {(s, e): AmiciResultWrapper.from_hdf(path) for s, e, path in failures},
        )


if __name__ == "__main__":
    # General settings
    recompile = True
    rerun = True
    n_samples = 250

    random_seed_value = 5_11_2021

    # Sampling settings
    # Number of parallel processes.
    n_parallel = 4
    # Maximum number of experiments that can fail before simulating the sample in
    # different experiments is stopped. Can be set to None to never stop early.
    maxfail = 1
    # Maximum time a parameter sample can take before the sample simulation
    # is stopped, can be set to None to never stop early.
    # Note: this is for per experiments, but all experiments for a parameter
    #       sample are timed together (n * timeout)
    sample_timeout = 1 * 60
    # TODO: Does not seem to actually do anything in amici?
    simulation_timeout = 60

    solver_settings = {
        "atol": 1e-12,
        "rtol": 1e-12,
        "sens": "forward",
        "order": "first",
        "nonnegative": False,
    }
    working_directory = pathlib.Path("results_exp_pred")

    # Set-up working directory & cache
    working_directory.mkdir(exist_ok=True)
    original_model_description_path = pathlib.Path("data/model.xlsx")
    model_description_path = working_directory / "model.xlsx"
    shutil.copy(original_model_description_path, model_description_path)

    # Save some settigns to identify the run later.
    settings = {
        "random_seed": random_seed_value,
        "n_samples": n_samples,
        "n_parallel": n_parallel,
        "maxfail": maxfail,
        "sample_timeout": sample_timeout,
        "simulation_timeout": simulation_timeout,
        **solver_settings,
    }
    settings_path = pathlib.Path(working_directory / "settings.json")
    settings_path.write_text(json.dumps(settings))

    # Build model based on active genes, reactions and other settings
    putida_native_genes = {"fcs", "ech", "curA", "vdh"}
    knockouts = {"ech", "curA", "vdh"}
    knockins = {"CURS1", "DCS", "COMT", "CCoAOMT"}  # , "C3H"}  #,  "TAL"}

    active_genes = (putida_native_genes - knockouts) | knockins
    active_reactions = set()
    # Ignore all cinnamic acid reactions for now.
    ignore_reactions = {
        "FCL_CIA",
        "COAHY_CIA",
        "DCS_CICOA",
        "CURS_DCIM",
        "CURS_CIFEM1",
        "CURS_CIFEM2",
        "CURS_CICUM1",
        "CURS_CICUM2",
    }
    # Fix all co-factors to a constant concentration.
    fixed = {
        "AMP",
        "ATP",
        "HCO3",
        "COA",
        "MACOA",
        "PPI",
    }
    if {"CCoAOMT", "COMT"} & knockins:
        fixed |= {"AHCYS", "AMET"}
    if "C3H" in knockins:
        fixed |= {"FAD", "FADH", "O2"}
    implicit = {"H2O", "H"}

    priors = {
        # Base quantities
        Parameters.mu: (-880.0, 680.00),
        # Bit decreased, since it's secondary metabolism.
        Parameters.kv: (1.0, 3),
        Parameters.km: (0.1, 3),
        Parameters.c: (0.1, 3),
        Parameters.u: (0.0001, 3),
        Parameters.ki: (0.1, 3),
        Parameters.ka: (0.1, 3),
    }
    pathway = Pathway(
        pathway_information_path=model_description_path,
        active={"genes": active_genes, "reactions": active_reactions},
        ignore={"reactions": ignore_reactions},
        priors=priors,
        simplifications={
            "enzyme_rate": True,
            # Fix parameters as constants
            # "constants": {"R": None, "T": None},
            # States that should not be added to the formula, except for the calculation of delta mu.
            "implicit_states": implicit,
            # Fix states as constant
            "fixed_states": fixed,
            # Fix reactions as constant (or other expression)
            "fixed_reactions": dict(),
        },
    )

    sbml_model = pathway.symbolic_model.to_sbml_model()
    sbml_model.initial_concentrations = {i: 1 for i in sbml_model.metabolites}
    sbml_model.parameter_values = {i: 1 for i in sbml_model.parameters}

    model_location = working_directory / "model.sbml"
    sbml_model.save(model_location)

    # TODO: The balancing method should be updated to reflect the catalysis of multiple
    #       reactions by a single enzmye. This results in the enzyme concentration being
    #       shared, while each of the reactions has a seperate velocity constant. Binding
    #       constants are partially shared in the case of common substrates. How does this
    #       work with the Q matrix?
    #       Anyway, for now we simply ignore this when taking samples.
    parameter_location = working_directory / "parameters.npz"
    pathway.balanced_parameters.save(parameter_location)
    pathway._used_parameter_data.df.to_csv(
        working_directory / "input_parameters.debug.csv"
    )
    pathway.balanced_parameters.to_frame(mean=True).T.to_csv(
        working_directory / "balanced_parameters.debug.csv"
    )

    # Add events to the model for adding extra substrates.
    events = {
        "addition": {
            "trigger": "time >= event_t_addition",
            "targets": ["CAA", "CUA", "FEA"],
            "assignments": [
                "CAA + event_c_addition_CAA",
                "CUA + event_c_addition_CUA",
                "FEA + event_c_addition_FEA",
            ],
            "parameters": [
                "event_t_addition",
                "event_c_addition_CAA",
                "event_c_addition_CUA",
                "event_c_addition_FEA",
            ],
        },
    }
    original_model_location, model_location = (
        model_location,
        working_directory / "model_events.sbml",
    )
    event_parameters = update_model_with_events(
        original_model_location, model_location, events
    )
    fixed_parameters = event_parameters + ["R", "T"]

    # Define and save experimental designs
    t_pulse_placeholders = [0, 1, 2]
    substrates = ["CAA", "CUA", "FEA"]
    initial_ammount = addition_ammount = 2
    factors = [
        {"name": "initial", "keys": substrates, "levels": [initial_ammount, 0]},
        {"name": "addition", "keys": substrates, "levels": [addition_ammount, 0]},
        {
            "name": "parameter",
            "keys": ["event_t_addition"],
            "levels": t_pulse_placeholders,
        }
        # {
        #     "name": "parameter",
        #     "keys": ["COMT"],
        #     # Note that NaN represents no change - i.e. sample as normal.
        #     "levels": [0, np.NaN],
        # },
    ]

    design = create_design(factors)
    # For the first run, just do the combinations of initial concentrations.
    design_round_1 = design.iloc[:7].copy().reset_index(drop=True)
    design_round_1.loc[:, pd.IndexSlice['addition', :]] = 0
    design_round_1.to_csv(working_directory / "design_round_1.csv")

    # Define and save measurements
    # TODO: Could be more flexible to allow for custom rules (i.e. 1.1 CUR + 0.9 DCUR)
    simulation_time = 250
    timepoints = np.linspace(0, simulation_time, 101)
    measured = ["CAA", "CUA", "FEA", "CUR", "DCUR", "BDCUR"]
    measurements = (
        pd.DataFrame(1, columns=measured, index=timepoints)
        .astype(bool)
        .rename_axis("timepoints")
    )
    measurements.to_csv(working_directory / "measurements.csv")

    # Compile model
    # TODO: This should be cached
    amici_directory = working_directory / "amici"
    model_name = "model"
    if recompile:
        module_directory = compile(
            model_location, model_name, amici_directory, measurements, fixed_parameters
        )
    else:
        module_directory = amici_directory / model_name

    # Sample parameter set
    reaction_enzyme_mapping_placeholder = {
        "4CL": ("FCL_CUA", "FCL_FEA", "FCL_CAA"),
        "CURS": ("CURS_CUR", "CURS_BDCUR", "CURS_DCUR1", "CURS_DCUR2"),
        "DCS": ("DCS_CUCOA", "DCS_FECOA"),
    }
    symbolic_model = symbolicSBML.SBMLModel(original_model_location)
    original_parameters = pd.Series(symbolic_model.parameter_values)

    # # Hack to decrease range of keq C3H, by changing the mu of FAD/FADH2
    # FAD_index = pathway.balanced_parameters.columns.index(('mu', 'FAD', ''))
    # FADH_index = pathway.balanced_parameters.columns.index(('mu', 'FADH', ''))

    # # Delta = approx 380 +- 6
    # # Delta - 400 = -20 works fine
    # # Delta - 300 =  80 has some failures (6%)
    # # Delta - 280 = 100 has many failures (96%)
    # # Delta - 250 = 130 has 100% failure rate
    # # Delta - 200 = 180 has 100% failure rate
    # pathway.balanced_parameters.q_post[FAD_index] += 145
    # pathway.balanced_parameters.q_post[FADH_index] -= 145

    with random_seed(random_seed_value):
        samples = sample_parameters(
            pathway.balanced_parameters,
            [i for i in symbolic_model.parameters if i not in fixed_parameters],
            n_samples,
            reaction_enzyme_mapping_placeholder,
        )
    # For debugging only.
    # with random_seed(random_seed_value):
    # samples_extended = pathway.balanced_parameters.sample(n_samples, False, True)

    parameter_sets = samples
    initial_concentrations = pd.Series(symbolic_model.initial_concentrations)
    initial_concentrations = (
        pd.concat([initial_concentrations] * n_samples, axis=1)
        .rename_axis(index="metabolite", columns="set")
        .T
    )
    initial_concentrations.loc[:, ["CAA", "CUA", "FEA"]] = 0
    initial_concentrations.loc[:, ["CACOA", "CUCOA", "FECOA"]] = 0
    initial_concentrations.loc[:, ["DCUCOA", "DFECOA"]] = 0
    initial_concentrations.loc[:, ["CUR", "DCUR", "BDCUR"]] = 0

    parameter_sets.to_csv(working_directory / "parameters.csv")

    simulations = create_simulations_from_design(
        parameter_sets,
        initial_concentrations,
        design_round_1,
        pathway,
    )
    output_directory = working_directory / "simulations"
    if rerun:
        print("Simulating data")
        output_directory.mkdir(exist_ok=False)
        data, failures = run_simulations(
            model_name=model_name,
            module_directory=module_directory,
            timepoints=timepoints,
            simulations=simulations,
            output_directory=output_directory,
            n_parallel=n_parallel,
            solver_settings=solver_settings,
            max_fail=maxfail,
            simulation_timeout=simulation_timeout,
            sample_timeout=sample_timeout,
            return_data=True,
        )
    else:
        print("Loading old data")
        data, failures = load_simulation_data(output_directory, len(design_round_1))

    single_substrate_experiments = design_round_1.loc[
        design_round_1.initial.sum(axis=1) == initial_ammount
    ].index
    t_half_met = ["CACOA", "CUCOA", "FECOA"]

    # Determine the time point where the concentrations are at half their maximum.
    t_half = {}
    for (sample, experiment), wrapper in data.items():
        # Only check single substrate experiments.
        if experiment not in single_substrate_experiments:
            continue
        x = wrapper.x[t_half_met].copy()
        t_max, x_max = x.idxmax(), x.max()

        # Remove the values before the peak
        x[(x.index.values[:, np.newaxis] < t_max.values)] = np.NaN

        # Get the clostet points to x_max/2, after t_max.
        t_half[(sample, experiment)] = (x - (x_max / 2)).abs().idxmin()

    # Delete data to save some memory.
    del data

    t_half = (
        pd.concat(t_half, axis=1)
        .T.rename_axis(["sample", "experiment"], axis=0)
        .sort_index()
    )

    # Design the pulse experiments around the time range that the
    # intermediates are at half concentration.
    # We ignore 0 values, since those are simulationts were the precursor is not provided.
    t_half_quantiles = t_half[t_half != 0].stack().quantile([0.25, 0.5, 0.75])
    # TODO: Could decide pulse also per experiment or metabolite, instead of lumping everything.

    # To limit the number of experiments, we only look at the single additions for now.
    # In addition we ignore designs with no initial concentrations, similar to round 1.
    designs_round_2 = design.loc[
        (design.addition.sum(axis=1) == addition_ammount)
        & (design.initial.sum(axis=1) > 0)
    ].reset_index(drop=True)

    designs_round_2.loc[
        :, pd.IndexSlice["parameter", "event_t_addition"]
    ] = designs_round_2.parameter.event_t_addition.replace(
        dict(zip(t_pulse_placeholders, t_half_quantiles.values))
    )
    designs_round_2.to_csv(working_directory / "design_round_2.csv")
    simulations_round_2 = create_simulations_from_design(
        parameter_sets,
        initial_concentrations,
        designs_round_2,
        pathway,
    )

    # pool_initiator(model_name, module_directory, timepoints, solver_settings)
    # experiment, sample, initial_concentrations, parameters = simulations_round_2[0]
    # rdata = run_experiment(model, solver, initial_concentrations, parameters)
    # wrapper = AmiciResultWrapper(rdata, model)

    output_directory_2 = working_directory / "simulations_2"
    if rerun:
        print("Simulating data round 2")
        output_directory_2.mkdir(exist_ok=False)
        run_simulations(
            model_name=model_name,
            module_directory=module_directory,
            timepoints=timepoints,
            simulations=simulations_round_2,
            output_directory=output_directory_2,
            n_parallel=n_parallel,
            solver_settings=solver_settings,
            max_fail=maxfail,
            simulation_timeout=simulation_timeout,
            # This is approx 10x more experiments than the previous run.
            sample_timeout=sample_timeout * 12,
            return_data=False,
        )
    # # Simulation success
    # t_fail = {}
    # for key, result in data.items():
    #     if result.diagnostics.status != amici.AMICI_SUCCESS:
    #         t_fail[key] = result.t[np.isnan(result.x).any(axis=1).argmax()]
    #     else:
    #         t_fail[key] = None
    # t_fail = (
    #     pd.Series(t_fail)
    #     .unstack(0)
    #     .rename_axis(index="parameter_set", columns="experiment")
    # )
    # success = t_fail.isna().all(axis=1)
    # print("Time of simulation failure (NaN => success)")
    # print(
    #     "Successfully simulated parameter sets: {}/{}".format(success.sum(), n_samples)
    # )
    # print(t_fail)
    # print()

    # example = next(iter(data.values()))

    # # Model shows completely different levels of sensitivity for different parameter types.
    # # Mu == 100 x rate = 1000 x km
    # ptypes = (
    #     ("All", ".*"),
    #     ("Cofactors (c)", "^c__"),
    #     ("Chemical energy", "^mu__"),
    #     ("Enzyme rate", "^(u|u_v|kv)__"),
    #     ("Binding constants", "^km__"),
    # )
    # print("Difference in FIM scale per parameter type.")
    # print("                   Trace   / Sum     / Mean    / Std")
    # for ptype, pat in ptypes:
    #     mask = example.parameters.index[example.parameters.str.contains(pat)]
    #     subset = example.FIM.iloc[mask, :].iloc[:, mask]
    #     print(
    #         "{:<17}: {:.1e} / {:.1e} / {:.1e} / {:.1e}".format(
    #             ptype,
    #             np.trace(subset),
    #             np.sum(subset.to_numpy()),
    #             np.mean(subset.to_numpy()),
    #             np.std(subset.to_numpy()),
    #         )
    #     )
    #     # plt.figure()
    #     # sns.heatmap(
    #     #     pd.DataFrame(subset, columns=pnames[mask], index=pnames[mask]),
    #     #     center=0,
    #     #     cmap="RdBu",
    #     #     xticklabels=True,
    #     #     yticklabels=True,
    #     # )
    # print()

    # # Find best experiment
    # # Since we don't know the parameters, we average the FIMs for each experiment.
    # # We skip parameter sets that are not succesfully simualted in all conditions.
    # per_experiment = {}
    # for i in design.index:
    #     stacked = np.stack([data[i, j].FIM for j in success.index[success]])
    #     mean, std = stacked.mean(axis=0), stacked.std(axis=0)
    #     per_experiment[i] = mean

    # # Find best set of n experiments according to some of the criteria.
    # def objective(FIMS):
    #     FIM = np.stack(FIMS).sum(axis=0)
    #     return tuple(FIM_measures(FIM, c) for c in ("A", "D", "E"))

    # best = {}
    # for n in range(1, 5):
    #     results = {}
    #     for combination in itertools.combinations(list(per_experiment.keys()), n):
    #         results[combination] = objective([per_experiment[i] for i in combination])
    #     results = pd.DataFrame(results, index=["A", "D", "E"]).T
    #     print("Optimal combination (n={})".format(n))
    #     print(results.loc[results.idxmax()])
    #     print()

    # plt.ion()

    # df = pd.DataFrame(rdata.FIM, columns=pnames, index=pnames)
    # sns.heatmap(
    #     df,
    #     center=0,
    #     cmap="RdBu",
    #     xticklabels=True,
    #     yticklabels=True,
    # )

    # diag = pd.Series(np.diag(df), pnames)
    # diag.index = diag.index.str.split('__', expand=True, n=1)
    # diag = diag.reset_index().set_axis(['ptype', 'specifier', 'value'], axis=1)
    # sns.displot(data=diag, hue='ptype', x='value', log_scale=10, kind='kde')

    # pd.DataFrame(np.log10(rdata.x), index=timepoints, columns=xnames).plot()

"""Functions to generate rate laws.

Currently allows for the following:
    - Modular rate laws (all types and parametrizations)
        - Regulation is currently not yet supported
    - Mass action (reversible and irreversible)

Also contains the corresponding SBO terms.

TODO:
    - Other rate laws
    - Modular rate law type per reaction
        - Rate law detection based on SBO term
    - Including regulation (as marked per SBO term)
    - Including enzyme cases:
        - Multiple enzymes per reaction
        - Multiple reactions per enzyme
        - Enzyme as SBML species instead of parameter
    - Including compartment volumes (when appriopriate)
        - Enzyme concentration vs count
    - Simplication of parameters:
        - kcat + enzyme concetration -> Vmax
        - kv + enzyme concentration -> Vmax*
        - Combined km for directing binding modular (DM) and power-law modular (PM) rate law
Author: Rik van Rosmalen
"""
import functools

import numpy as np
import sympy

from .parameters import Parameters


# Cached version of Parameters.as_symbol, with added support for getting c_p = c* = c / km
@functools.lru_cache()
def get_symbol(
    parameter, metabolite=None, reaction=None, enzyme=None, set_assumptions=True
):
    """Get the parameter as a sympy symbol with the right identifiers.

    c and c_p are special cased, since we don't want to use c as a parameter
    and c_p is non_default. Cached version for efficiency.
    """
    if parameter == "c_p":
        return get_symbol(
            Parameters.c, metabolite, reaction, enzyme, set_assumptions
        ) / get_symbol(Parameters.km, metabolite, reaction, enzyme, set_assumptions)
    if parameter == Parameters.c:
        if set_assumptions:
            return sympy.Symbol(metabolite, real=True, nonnegative=True)
        else:
            return sympy.Symbol(metabolite)
    else:
        return Parameters.as_symbol(
            parameter, metabolite, reaction, enzyme, set_assumptions
        )


def _modular_binding_states(metabolites, rate_law, set_assumptions=True):
    products = [
        (get_symbol(Parameters.c, m, set_assumptions), c)
        for m, c in metabolites.items()
        if c > 0
    ]
    reactants = [
        (get_symbol(Parameters.c, m, set_assumptions), abs(c))
        for m, c in metabolites.items()
        if c < 0
    ]

    # Each molecule binds and unbinds seperatly, but not at the same time.
    if rate_law == "CM":
        return (
            sympy.Mul(*((1 + m) ** c for m, c in reactants))
            + sympy.Mul(*((1 + m) ** c for m, c in products))
            - 1
        ).expand()
    # Substrates bind together and products bind together, but not at the same time.
    elif rate_law == "DM":
        return (
            sympy.Mul(*(m ** c for m, c in reactants))
            + sympy.Mul(*(m ** c for m, c in products))
            + 1
        ).expand()
    # Each molecule binds and unbinds seperatly, also at the same time.
    elif rate_law == "SM":
        return sympy.Mul(*((1 + m) ** c for m, c in reactants + products)).expand()
    elif rate_law == "FM":
        raise ValueError("FM rate law not based on enzyme kinetics.")
    elif rate_law == "PM":
        return 1
    else:
        raise ValueError("Invalid rate law: {}".format(rate_law))


def _get_additional_binding_states(main, others, rate_law, set_assumptions=True):
    # No enzyme binding assumed in PM rate law.
    if rate_law == "PM":
        return {}

    # Extract the coefficients of the binding states
    binding_states = _modular_binding_states(
        main, rate_law, set_assumptions
    ).as_coefficients_dict()
    other_states = [
        _modular_binding_states(i, rate_law, set_assumptions).as_coefficients_dict()
        for i in others
    ]
    all_states = other_states + [binding_states]

    # Get the maximum for each binding state
    max_coefficient = {}
    for state in {key for i in all_states for key in i.keys()}:
        max_coefficient[state] = max(i[state] for i in all_states)

    # Substract the main state from the maximum.
    return {
        k: v - binding_states[k]
        for k, v in max_coefficient.items()
        if (v - binding_states[k]) != 0
    }


def _modular_substitutions(
    allowed_substitutions,
    parametrisation,
    rate_law,
    h,
    reactants,
    products,
    reaction,
    enzyme,
    set_assumptions,
):
    """Prepare substitution dictionairy based on parametrisation and rate law.

    TODO: Case where we want to merge km + rate but keep u seperate.
    TODO: Several buggy cases (km_merged and km_rate + PM + cat)
    """
    substitutions = {}
    reaction_kwargs = {
        "metabolite": None,
        "reaction": reaction,
        "enzyme": enzyme,
        "set_assumptions": set_assumptions,
    }
    metabolite_kwargs = {
        "reaction": reaction,
        "enzyme": enzyme,
        "set_assumptions": set_assumptions,
    }

    u = get_symbol(Parameters.u, **reaction_kwargs)
    kv = get_symbol(Parameters.kv, **reaction_kwargs)
    kcat_prod = get_symbol(Parameters.kcat_prod, **reaction_kwargs)
    kcat_sub = get_symbol(Parameters.kcat_sub, **reaction_kwargs)

    if {"enzyme_rate", "km_merged", "km_rate"} & allowed_substitutions:
        # hal and weg use kV + u => u_v
        if parametrisation in {"hal", "weg"}:
            u_v = get_symbol(Parameters.u_v, **reaction_kwargs)
            substitutions[kv * u] = u_v
        # cat uses kcat+- + u => vmax+-
        elif parametrisation == "cat":
            vmax_prod = get_symbol(Parameters.vmax_prod, **reaction_kwargs)
            substitutions[kcat_prod * u] = vmax_prod

            vmax_sub = get_symbol(Parameters.vmax_sub, **reaction_kwargs)
            substitutions[kcat_sub * u] = vmax_sub

    if {"km_merged", "km_rate"} & allowed_substitutions:
        if rate_law not in {"DM", "FM", "PM"}:
            raise ValueError(
                "Invalid substitution ({}) for sub rate law {}.".format(
                    "km_merged", rate_law
                )
            )
        if "km_rate" in allowed_substitutions and rate_law != "PM":
            raise ValueError(
                "Invalid substitution ({}) for sub rate law {}.".format(
                    "km_rate", rate_law
                )
            )
        # Gather product and substrate terms into two groups
        # Don't forget to incorporate the cooperativity and stoichiometry!
        km_prod_terms = sympy.Mul(
            *(
                get_symbol(Parameters.km, met, **metabolite_kwargs) ** (s * h)
                for met, s in products
            )
        )
        km_sub_terms = sympy.Mul(
            *(
                get_symbol(Parameters.km, met, **metabolite_kwargs) ** (s * h)
                for met, s in reactants
            )
        )

        if (
            rate_law == "PM"
            and parametrisation in {"weg", "hal"}
            and "km_rate" in allowed_substitutions
        ):
            # For PM + weg / hal we can merge the km + u_v
            km_tot_rate = get_symbol(Parameters.km_tot_rate, **reaction_kwargs)
            expr = (
                Parameters.kv * Parameters.u / sympy.sqrt(km_sub_terms * km_prod_terms)
            )
            substitutions[expr] = km_tot_rate
            # Remove the more simple substitution
            del substitutions[Parameters.kv * Parameters.u]

        elif rate_law == "PM" and "km_rate" in allowed_substitutions:
            raise NotImplementedError("Buggy implementation: TODO")
            # For PM + kcat we can merge vmax+ + km_sub and vmax- + km_prod
            km_prod_rate = get_symbol(Parameters.km_prod_rate, **reaction_kwargs)
            expr = Parameters.kcat_prod * Parameters.u / km_prod_terms
            substitutions[expr] = km_prod_rate
            del substitutions[P[Parameters.kcat_prod] * P[Parameters.u]]

            km_sub_rate = get_symbol(Parameters.km_sub_rate, **reaction_kwargs)
            expr = Parameters.kcat_sub * Parameters.u / km_sub_terms
            substitutions[expr] = km_sub_rate
            del substitutions[P[Parameters.kcat_sub] * P[Parameters.u]]

        elif (
            rate_law in {"PM", "FM"}
            and parametrisation in {"weg", "hal"}
            and "km_rate" not in allowed_substitutions
        ):
            if parametrisation == "hal" or rate_law == "FM":
                raise NotImplementedError("Buggy implementation: TODO")
            # For FM / PM + weg / hal we can still merge all the kms
            km_tot = Parameters.as_symbol(
                Parameters.km_tot, None, reaction, set_assumptions
            )
            substitutions[km_sub_terms * km_prod_terms] = km_tot
            # This might be easier to substitute instead.
            substitutions[
                1 / sympy.sqrt(km_sub_terms * km_prod_terms)
            ] = 1 / sympy.sqrt(km_tot)

        elif "km_rate" not in allowed_substitutions:
            raise NotImplementedError("Buggy implementation: TODO")
            # Final case: just merge the kms for products and substrates seperately.
            # This holds for:
            #   DM + weg / hal / cat
            #   PM / FM + cat
            substitutions[km_prod_terms] = km_prod
            substitutions[1 / sympy.sqrt(km_prod_terms)] = 1 / sympy.sqrt(km_prod)
            substitutions[km_sub_terms] = km_sub
            substitutions[1 / sympy.sqrt(km_sub_terms)] = 1 / sympy.sqrt(km_sub)
        else:
            raise ValueError("Invalid combinatation of substitutions and rate law.")
    return substitutions


def _modular_numerator(
    parametrisation,
    h,
    reactants,
    products,
    metabolites,
    reaction,
    enzyme,
    ignore_thermodynamics_mu=None,
    ignore_thermodynamics_c=None,
    set_assumptions=True,
):
    if ignore_thermodynamics_mu is None:
        ignore_thermodynamics_mu = []
    if ignore_thermodynamics_c is None:
        ignore_thermodynamics_c = []

    reaction_kwargs = {
        "metabolite": None,
        "reaction": reaction,
        "enzyme": enzyme,
        "set_assumptions": set_assumptions,
    }
    metabolite_kwargs = {
        "reaction": reaction,
        "enzyme": enzyme,
        "set_assumptions": set_assumptions,
    }

    if parametrisation == "cat":
        kcat_prod = get_symbol(Parameters.kcat_prod, **reaction_kwargs)
        kcat_sub = get_symbol(Parameters.kcat_sub, **reaction_kwargs)
        numerator = kcat_sub * sympy.Mul(
            *(
                get_symbol("c_p", met, **metabolite_kwargs) ** (s * h)
                for met, s in reactants
                if met not in ignore_thermodynamics_c
            )
        ) - kcat_prod * sympy.Mul(
            *(
                get_symbol("c_p", met, **metabolite_kwargs) ** (s * h)
                for met, s in products
                if met not in ignore_thermodynamics_c
            )
        )
    elif parametrisation == "hal":
        kv = get_symbol(Parameters.kv, **reaction_kwargs)
        keq = get_symbol(Parameters.keq, **reaction_kwargs)

        numerator = (
            kv
            * (
                sympy.sqrt(keq ** h)
                * sympy.Mul(
                    *(
                        get_symbol(Parameters.c, met, **metabolite_kwargs) ** (s * h)
                        for met, s in reactants
                        if met not in ignore_thermodynamics_c
                    )
                )
                - sympy.sqrt(keq ** -h)
                * sympy.Mul(
                    *(
                        get_symbol(Parameters.c, met, **metabolite_kwargs) ** (s * h)
                        for met, s in products
                        if met not in ignore_thermodynamics_c
                    )
                )
            )
            / sympy.sqrt(
                sympy.Mul(
                    *(
                        get_symbol(Parameters.km, met, **metabolite_kwargs)
                        ** (sympy.Abs(s) * h)
                        for met, s in metabolites.items()
                        if met not in ignore_thermodynamics_c
                    )
                )
            )
        )
    elif parametrisation == "weg":
        kv = get_symbol(Parameters.kv, **reaction_kwargs)
        R = get_symbol(Parameters.R)
        T = get_symbol(Parameters.T)

        numerator = (
            kv
            * (
                sympy.exp(
                    -(
                        h
                        * sympy.Add(
                            *(
                                get_symbol(Parameters.mu, met, **metabolite_kwargs) * s
                                for met, s in metabolites.items()
                                if met not in ignore_thermodynamics_mu
                            )
                        )
                    )
                    / (2 * R * T)
                )
                * sympy.Mul(
                    *(
                        get_symbol(Parameters.c, met, **metabolite_kwargs) ** (s * h)
                        for met, s in reactants
                        if met not in ignore_thermodynamics_c
                    )
                )
                - sympy.exp(
                    (
                        h
                        * sympy.Add(
                            *(
                                get_symbol(Parameters.mu, met, **metabolite_kwargs) * s
                                for met, s in metabolites.items()
                                if met not in ignore_thermodynamics_mu
                            )
                        )
                    )
                    / (2 * R * T)
                )
                * sympy.Mul(
                    *(
                        get_symbol(Parameters.c, met, **metabolite_kwargs) ** (s * h)
                        for met, s in products
                        if met not in ignore_thermodynamics_c
                    )
                )
            )
            / sympy.sqrt(
                sympy.Mul(
                    *(
                        get_symbol(Parameters.km, met, **metabolite_kwargs)
                        ** (sympy.Abs(s) * h)
                        for met, s in metabolites.items()
                        if met not in ignore_thermodynamics_c
                    )
                )
            )
        )
    else:
        raise ValueError(
            "Invalid numerator parametrisation for modular "
            "rate law: {}. Must be one of {{cat, hal, weg}}".format(parametrisation)
        )
    return numerator


def _modular_denominator(
    rate_law,
    h,
    reactants,
    products,
    metabolites,
    reaction,
    enzyme,
    set_assumptions=True,
):
    """Construct denominator."""
    metabolite_kwargs = {
        "reaction": reaction,
        "enzyme": enzyme,
        "set_assumptions": set_assumptions,
    }
    if rate_law == "CM":
        denominator = (
            sympy.Mul(
                *(
                    (1 + get_symbol("c_p", met, **metabolite_kwargs)) ** (s * h)
                    for met, s in reactants
                )
            )
            + sympy.Mul(
                *(
                    (1 + get_symbol("c_p", met, **metabolite_kwargs)) ** (s * h)
                    for met, s in products
                )
            )
            - 1
        )
    elif rate_law == "SM":
        denominator = sympy.Mul(
            *(
                (1 + get_symbol("c_p", met, **metabolite_kwargs)) ** (sympy.Abs(s) * h)
                for met, s in metabolites.items()
            )
        )
    elif rate_law == "DM":
        denominator = (
            sympy.Mul(
                *(
                    get_symbol("c_p", met, **metabolite_kwargs) ** (s * h)
                    for met, s in reactants
                )
            )
            + sympy.Mul(
                *(
                    get_symbol("c_p", met, **metabolite_kwargs) ** (s * h)
                    for met, s in products
                )
            )
            + 1
        )
    elif rate_law == "FM":
        denominator = sympy.Mul(
            *(
                get_symbol("c_p", met, **metabolite_kwargs) ** (sympy.Abs(s) * h / 2)
                for met, s in metabolites.items()
            )
        )
    elif rate_law == "PM":
        denominator = 1
    else:
        raise ValueError(
            "Invalid denominator rate law for modular "
            "rate law: {}. Must be on of {{CM, SM, DM, FM, PM}}".format(rate_law)
        )
    return denominator


def _modular_regulation(regulation, reaction, enzyme, set_assumptions):
    f_reg = 1
    D_reg = 0
    for (metabolite, regulation_type, direction) in regulation:
        if direction not in ("activation", "inhibition"):
            raise ValueError(
                "Regulation should be activation or inhibition, not: {}".format(
                    direction
                )
            )

        c = get_symbol(Parameters.c, metabolite, reaction, enzyme, set_assumptions)
        kia = get_symbol(
            Parameters.ki if direction == "inhibition" else Parameters.ka,
            metabolite,
            reaction,
            enzyme,
            set_assumptions,
        )

        if regulation_type == "specific":
            wp = get_symbol(
                Parameters.w_plus_p
                if direction == "inhibition"
                else Parameters.w_minus_p,
                metabolite,
                reaction,
                enzyme,
                set_assumptions,
            )

            if direction == "inhibition":
                term = (kia / c) ** wp
            else:
                term = (c / kia) ** wp

            D_reg += term
        elif regulation_type in ("partial", "complete"):
            w = get_symbol(
                Parameters.w_plus if direction == "inhibition" else Parameters.w_minus,
                metabolite,
                reaction,
                enzyme,
                set_assumptions,
            )

            if regulation_type == "complete":
                pia = 0
            else:
                pia = get_symbol(
                    Parameters.pi if direction == "inhibition" else Parameters.pa,
                    metabolite,
                    reaction,
                    enzyme,
                    set_assumptions,
                )

            if direction == "inhibition":
                term = (pia + (((1 - pia) * (c / kia)) / (1 + c / kia))) ** w
            else:
                term = (pia + ((1 - pia) / (1 + c / kia))) ** w

            f_reg *= term
        else:
            raise ValueError("Invalid type of regulation: {}".format(regulation_type))

    return f_reg, D_reg


def modular_rate_law(
    reaction,
    metabolites,
    parametrisation="cat",
    rate_law="CM",
    regulation=None,
    shared_enzyme=None,
    cooperativitiy=1,
    substitutions=None,
    set_assumptions=False,
    return_parts=False,
    enzyme=None,
    ignore_participants=None,
):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]
    :param parametrisation: Type of parametrisation: {cat, hal or weg}.
    :type parametrisation: str
    :param rate_law: Type of denominator term: {CM, SM, DM, FM or PM}.
    :type rate_law: str
    :param regulation: List of regulators for this reaction.
                       Each item should list: (metabolite, specific | complete | partial, activation | inhibition)
    :type regulation: list[(str, str, str)]
    :param shared_enzyme: For enzymes catalyzing more then one reaction, the extra binding states
                          can be compensated for by providing the stoichiometry of the other reactions.
    :type shared_enzyme: list[dict[str: int]]
    :param cooperativitiy: The cooperativity factor, defaults to 1.
    :type cooperativitiy: number / symbol / str (will be converted to symbol)
    :param substitutions: Allowed subsitutions.
    :type substitutions: iterable of {'enzyme_rate', 'km_merged', 'km_rate'} or 'all'
    Note that km_rate implies km_merged and u_v / v_max.
    :param set_assumptions: Set assumptions on the sympy variables (real values for all
    metabolites concentrations and parameters, positive for a non-logarithmic parameters and
    metabolite concentrations.)
    :type set_assumptions: bool, (default=False)
    :param enzyme: If the enzyme is given, the enzyme name is used for parameters where appropriate.
    :type enzyme: str | None, (default=None)

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression

    :raises ValueError: Raises a ValueError if an invalid parametrisation
    or sub_rate_law is passed.

    """
    possible_parametrisations = {"cat", "hal", "weg"}
    parametrisation = parametrisation.lower()
    if parametrisation not in possible_parametrisations:
        raise ValueError("Invalid parametrisation: {}".format(parametrisation))

    possible_rate_laws = {"CM", "SM", "DM", "FM", "PM"}
    rate_law = rate_law.upper()
    if rate_law not in possible_rate_laws:
        raise ValueError("Invalid rate law: {}".format(rate_law))

    possible_substitutions = {"enzyme_rate", "km_merged", "km_rate"}
    if substitutions == "all":
        allowed_substitutions = possible_substitutions
    elif substitutions is None:
        allowed_substitutions = set()
    elif set(substitutions) - possible_substitutions:
        raise ValueError(
            "Invalid substitutions: {}".format(
                set(substitutions) - possible_substitutions
            )
        )
    else:
        allowed_substitutions = set(substitutions)

    h = sympy.sympify(cooperativitiy)

    # Ignore certain participants
    ignore_in_binding_states = set()
    ignore_in_thermodynamics_mu = set()
    ignore_in_thermodynamics_c = set()
    for metabolite, where in ignore_participants:
        if where == "binding":
            ignore_in_binding_states.add(metabolite)
        elif where == "thermodynamics_mu":
            if parametrisation != "weg":
                raise ValueError("Ignore in mu only works with weg parametrisation.")
            ignore_in_thermodynamics_mu.add(metabolite)
        elif where == "thermodynamics_c":
            ignore_in_thermodynamics_c.add(metabolite)
        else:
            raise ValueError

    # Assign a default to enzyme name if not given.
    if enzyme is None:
        enzyme = "E_{}".format(reaction)

    # Helpers
    reactants = [(m_id, abs(s)) for m_id, s in metabolites.items() if s < 0]
    products = [(m_id, s) for m_id, s in metabolites.items() if s > 0]

    u = get_symbol(
        Parameters.u, reaction=reaction, enzyme=enzyme, set_assumptions=set_assumptions
    )
    numerator = _modular_numerator(
        parametrisation,
        h,
        reactants,
        products,
        metabolites,
        reaction,
        enzyme,
        ignore_in_thermodynamics_mu,
        ignore_in_thermodynamics_c,
        set_assumptions=set_assumptions,
    )

    denominator = _modular_denominator(
        rate_law,
        h,
        [i for i in reactants if i[0] not in ignore_in_binding_states],
        [i for i in products if i[0] not in ignore_in_binding_states],
        metabolites,
        reaction,
        enzyme,
        set_assumptions=set_assumptions,
    )

    if regulation is not None:
        f_reg, D_reg = _modular_regulation(
            regulation, reaction, enzyme, set_assumptions=set_assumptions
        )
    else:
        f_reg, D_reg = 1, 0

    if shared_enzyme:
        # Get the missing states
        shared_enzyme = [
            {k: v for k, v in enzyme.items() if k not in ignore_in_binding_states}
            for enzyme in shared_enzyme
        ]
        missing_states_from_other_reactions = _get_additional_binding_states(
            {k: v for k, v in metabolites.items() if k not in ignore_in_binding_states},
            shared_enzyme,
            rate_law,
            set_assumptions=set_assumptions,
        )
        # Combine to a single term
        D_q = sympy.Add(
            *(
                state * count
                for state, count in missing_states_from_other_reactions.items()
            )
        )
        # Substitute the concentrations for c/km terms and include the cooperativity.
        km_terms = {
            met: get_symbol(
                "c_p", met.name, reaction, enzyme, set_assumptions=set_assumptions
            )
            ** h
            for met in D_q.free_symbols
        }
        D_q = D_q.subs(km_terms)
    else:
        D_q = 0

    final_expression = u * f_reg * numerator / (denominator + D_reg + D_q)

    # Apply any substitutions
    if substitutions is not None:
        if ignore_participants and {"km_merged", "km_rate"} & substitutions:
            raise NotImplementedError(
                "Substitutions km_merged and km_rate not updated yet to ignore metabolites."
            )
        substitutions = _modular_substitutions(
            allowed_substitutions,
            parametrisation,
            rate_law,
            h,
            reactants,
            products,
            reaction,
            enzyme,
            set_assumptions=set_assumptions,
        )
        final_expression = final_expression.subs(substitutions)
    # Simplication might be nice, but in can be quite slow, so leave it to the caller.
    if return_parts:
        return {
            "expression": final_expression,
            "substitutions": substitutions,
            "f_reg": f_reg,
            "D_reg": D_reg,
            "Dq": D_q,
            "numerator": numerator,
            "denominator": denominator,
        }
    return final_expression


def rev_mass_action_rate_law(reaction, metabolites):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression
    """
    kfw = sympy.Symbol(Parameters.separator.join(("k_fw", reaction)))
    krv = sympy.Symbol(Parameters.separator.join(("k_rv", reaction)))

    # Note that we need to take -s for reactants,
    # since we always assume the stoichiometry to be positive.
    reactants = [(m_id, -s) for m_id, s in metabolites.items() if s < 0]
    products = [(m_id, s) for m_id, s in metabolites.items() if s > 0]

    con = {}
    for term_id in metabolites.keys():
        con[term_id] = sympy.Symbol(term_id)

    fw_rate = kfw * sympy.Mul(*(con[i] ** s for i, s in reactants))
    rv_rate = krv * sympy.Mul(*(con[i] ** s for i, s in products))

    return fw_rate - rv_rate


def irrev_mass_action_rate_law(reaction, metabolites):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression
    """
    k = sympy.Symbol(Parameters.separator.join(("k", reaction)))

    # Note that we need to take -s for reactants,
    # since we always assume the stoichiometry to be positive.
    reactants = [(m_id, -s) for m_id, s in metabolites.items() if s < 0]

    con = {}
    for term_id in metabolites.keys():
        con[term_id] = sympy.Symbol(term_id)

    return k * sympy.Mul(*(con[i] ** s for i, s in reactants))


available_rate_laws = {
    "modular": modular_rate_law,
    "reversible mass action": rev_mass_action_rate_law,
    "irreversible mass action": irrev_mass_action_rate_law,
    "mass action": rev_mass_action_rate_law,
}

# TODO:
# convenience kinetics
# Linlog / loglin
# Hill type
# Michaelis menten


rate_law_sbo = {
    "default": "SBO:0000001",
    "modular": "SBO:0000527",
    "common modular": "SBO:0000528",
    "direct binding modular": "SBO:0000529",
    "force-dependent modular": "SBO:0000532",
    "power-law modular": "SBO:0000531",
    "simultaneous binding modular": "SBO:0000530",
    "reversible mass action": "SBO:0000042",
    "irreversible mass action": "SBO:0000041",
    "mass action": "SBO:0000012",
}


def generate_rate_laws(
    metabolites, reactions, stoichiometry, rate_law_type, **rate_law_args
):
    """Create a rate law term for a reaction.

    :param metabolites: List of metabolite names. (Same order as the stoichiometry.)
    :type metabolites: list[str]
    :param reactions: List of reaction names. (Same order as the stoichiometry.)
    :type reactions: list[str]
    :param stoichiometry: Stoichiometry matrix
    :type stoichiometry: (M, R) array
    :param rate_law_type: The type of rate law.
    :type rate_law_type: str
    :param rate_law_args: Extra arguments for the specific rate law.
    :type rate_law_args: dict

    :returns: A dictionairy of sympy expression for the rate laws.
    :rtype: dict[str: sympy expression]
    :returns: A list of parameters contained in those rate laws.
    :rtype: list[sympy expression]
    """
    try:
        rate_law_function = available_rate_laws[rate_law_type]
    except KeyError:
        raise ValueError(
            "Could not find a constuctor for: {}.\nChoose one of: {}".format(
                rate_law_type, ", ".join(available_rate_laws.keys())
            )
        )

    kinetics = {}
    parameters = set()
    for i, reaction in enumerate(reactions):
        row = stoichiometry[:, i]
        index = np.where(row != 0)[0]

        counts = [int(i) if int(i) == i else i for i in row[index]]
        metabolite_names = [metabolites[i] for i in index]
        mets = dict(zip(metabolite_names, counts))

        flux_term = rate_law_function(reaction, mets, **rate_law_args)
        kinetics[reaction] = flux_term
        parameters |= {i.name for i in flux_term.free_symbols}

    return kinetics, sorted(parameters - set(metabolites))

"""Model to work with the symbolic expressions of the kinetics model.

While the SBMLModel deals directly with the SBML, this is a bit higher level.
Models can be constructed in several ways:
    - Manually specify all metabolites, reactions, parameters, flux terms etc.
    - Manually specify the structural elements, but have the rate laws automatically generated.
    - Load the structure of an SBML model, and use automatically generated rate laws
    - Load the structure and kinetics of an SBML model.

The model can also be converted back to an SBML model, or saved to disk as SBML file.

Finally, the model can be used to:
    - Generate derived model quantities such as the Jacobian.
    - Use Sympy code generation to export the model equations as code in various languages.
      (Matlab/Octave - C - Fortran - Julia)

Author: Rik van Rosmalen

# TODO: When a model is updated in place, provide a convenient way to update the parameters etc.
"""
from __future__ import division, print_function

import collections

import numpy as np
import sympy

from .sbml import SBMLModel
from .rate_laws import generate_rate_laws


class SymbolicModel(object):
    def __init__(
        self,
        metabolites,
        reactions,
        parameters,
        stoichiometry,
        fluxes=None,
        observables=None,
    ):
        """Create a symbolic model.

        :param metabolites: List of metabolite names
        :type metabolites: list[str]
        :param reactions: List of reaction names
        :type reactions: list[str]
        :param parameters: List of parameter names
        :type parameters: list[str]
        :param stoichiometry: Stoichiometry matrix
        (must be in the same order as the metabolites and the reactions)
        :type stoichiometry: (M, R) array
        :param fluxes: Sympy expressions for each reaction. Missing fluxes will be set to 0.
        :type fluxes: dict[str: sympy expression]
        :param observables: Sympy expressions for each observable. (Optional)
        :type observables: dict[str: sympy expression]
        """
        self.reactions = reactions
        self.metabolites = metabolites
        self.parameters = parameters
        self.observables = collections.OrderedDict()
        if observables is not None:
            for key in observables:
                self.observables[key] = observables[key]

        # Try to convert to integers if possible, this will ease sympy.simplify.
        s_int = np.array(stoichiometry, dtype=int)
        if np.all(stoichiometry == s_int):
            stoichiometry = s_int
        self.stoichiometry = sympy.Matrix(stoichiometry)

        if fluxes is None:
            fluxes = {}
        self.fluxes = sympy.Matrix([fluxes.get(r, 0) for r in self.reactions])

        self.recreate_symbols()

        self.verify()

        # Lazy load these using properties
        self._dydt = None
        self._jacobian = None
        self._flux_sensitivity = None
        self._sensitivity = None

        # This only exists if the model is loaded from an SBML model.
        self.sbml_model = None

    def recreate_symbols(self):
        # Add t as an extra symbol
        self.t_symbol = sympy.symbols("t")

        # Create replacement mappings each metabolite and parameter with a matrix symbol.
        self.metabolite_symbols = sympy.symbols(self.metabolites)
        self._met_ms = sympy.MatrixSymbol("y", len(self.metabolites), 1)
        self._replace_met = dict(zip(self.metabolite_symbols, self._met_ms))

        # For parameters, make sure to load the parameters with the correct assumptions
        # by loading the parameters from the equations.
        mapping = {i.name: i for i in self.fluxes.free_symbols}
        self.parameter_symbols = []
        for parameter in self.parameters:
            if parameter in mapping:
                self.parameter_symbols.append(mapping[parameter])
            else:
                self.parameter_symbols.append(sympy.Symbol(parameter))
        self._par_ms = sympy.MatrixSymbol("p", len(self.parameters), 1)
        self._replace_par = dict(zip(self.parameter_symbols, self._par_ms))

    def verify(self):
        """Verify some assumptions about the data structure.

        :raises ValueError: A ValueError is raised if on of several assumptions
        about the internal structure is violated.
        """
        # Check sizes of stoichiometry and named metabolites/reactions
        n_m, n_r = self.stoichiometry.shape
        if n_m != len(self.metabolites):
            raise ValueError("Stoichiometry and metabolites are of different size.")

        if n_r != len(self.reactions):
            raise ValueError("Stoichiometry and reactions are of different size.")

        # No overlap can exist between parameters and metabolites
        overlap = set(self.metabolite_symbols) & set(self.parameter_symbols)
        if overlap:
            raise ValueError(
                "Overlapping parameters and metabolites: {}".format(overlap)
            )

        # All symbols should be parameters, metabolites or time
        valid_symbols = (
            set(self.metabolite_symbols) | set(self.parameter_symbols) | {self.t_symbol}
        )
        flux_symbols = self.fluxes.free_symbols
        if self.observables:
            obs_symbols = set.union(
                *(i.free_symbols for i in self.observables.values())
            )
        else:
            obs_symbols = set()
        used_symbols = flux_symbols | obs_symbols
        if not used_symbols <= valid_symbols:
            undefined = used_symbols - valid_symbols
            raise ValueError("Undefined symbols: {}".format(undefined))

    @classmethod
    def from_structure(
        cls, metabolites, reactions, stoichiometry, rate_law_type, **rate_law_args
    ):
        """Create a symbolic model based on the structure and standardized rate-laws.

        :param metabolites: List of metabolite names
        :type metabolites: list[str]
        :param reactions: List of reaction names
        :type reactions: list[str]
        :param stoichiometry: Stoichiometry matrix
        :type stoichiometry: (M, R) array
        :param rate_law_type: Type of rate-law to use.
        :type rate_law_type: str
        :param **rate_law_args: Settings to be passed down to rate-law construction.
        :type **rate_law_args:
        """
        fluxes, parameters = generate_rate_laws(
            metabolites, reactions, stoichiometry, rate_law_type, **rate_law_args
        )
        return cls(metabolites, reactions, parameters, stoichiometry, fluxes)

    @classmethod
    def from_sbml(cls, filepath, ignore_reactions=None, ignore_exchanges=False):
        """Create a symbolic model from an SBML file.

        :param filepath: Location to read the SBML file.
        :type filepath: str
        :param ignore_reactions: list of reaction identifiers to ignore when building the model.
        :type ignore_reactions: list[str]
        """
        model = SBMLModel(
            filepath,
            ignore_reactions=ignore_reactions,
            ignore_exchanges=ignore_exchanges,
        )
        self = cls(
            model.metabolites,
            model.reactions,
            model.parameters,
            model.stoichiometry,
            model.fluxes,
            model.observable_terms,
        )
        self.sbml_model = model
        return self

    @classmethod
    def from_sbml_structure(
        cls,
        filepath,
        ignore_reactions=None,
        ignore_exchanges=False,
        rate_law_type="modular",
        **rate_law_args
    ):
        """Create a symbolic model from an SBML file using the structure and standardized rate-laws.

        :param filepath: Location to read the SBML file.
        :type filepath: str
        :type filepath: str
        :param ignore_reactions: list of reactions
        :param rate_law_type: Type of rate law to use.
        Defaults to 'modular' for the Modular Rate Laws
        :type rate_law_type: str
        :param **rate_law_args: Settings to be passed down to rate-law construction.
        :type **rate_law_args:
        """
        model = SBMLModel(
            filepath,
            ignore_reactions=ignore_reactions,
            ignore_exchanges=ignore_exchanges,
        )
        self = cls.from_structure(
            model.metabolites,
            model.reactions,
            model.stoichiometry,
            rate_law_type,
            **rate_law_args
        )
        self.sbml_model = model
        return self

    def to_sbml_model(self):
        """Generate a minimal SBMLModel from the SymbolicModel."""
        stoichiometry = np.array(self.stoichiometry.tolist(), dtype=float)
        model = SBMLModel.from_structure(
            self.metabolites, self.reactions, stoichiometry
        )
        for reaction, flux in zip(self.reactions, self.fluxes):
            model.fluxes[reaction] = flux
        model.parameters = list(self.parameters)
        model.observables = list(self.observables.keys())
        model.observable_terms = self.observables
        return model

    def to_sbml(self, filepath):
        """Generate a minimal SBML file from the SymbolicModel.

        :param filepath: Location to save the SBML file.
        :param filepath: str
        """
        model = self.to_sbml_model()
        model.save(filepath)

    @property
    def dydt(self):
        """Generate the ODE update function (dydt = rhs)."""
        if self._dydt is None:
            self._dydt = sympy.Matrix(
                sympy.Matrix(self.stoichiometry) * sympy.Matrix(self.fluxes)
            )
        return self._dydt

    @property
    def sensitivity(self):
        """Generate the first order parametric sensitivity: dy/dp."""
        if self._sensitivity is None:
            self._sensitivity = sympy.Matrix(self.dydt).jacobian(self.parameter_symbols)
        return self._sensitivity

    @property
    def flux_sensitivity(self):
        """Generate the first order parametric sensitivity of each flux term: df/dp."""
        if self._flux_sensitivity is None:
            self._flux_sensitivity = sympy.Matrix(self.fluxes).jacobian(
                self.parameter_symbols
            )
        return self._flux_sensitivity

    @property
    def jacobian(self):
        """Generate the Jacobian: dy/dx."""
        if self._jacobian is None:
            self._jacobian = sympy.Matrix(self.dydt).jacobian(self.metabolite_symbols)
        return self._jacobian

    def _sym_matrix_replace(self, expr):
        """Return a version of expr with parameters and metabolites replaced by matrix symbols."""
        return expr.xreplace(self._replace_met).xreplace(self._replace_par)

    def _create_matrix_expression(self, variable, expr):
        """Return a version of expr as an matrix equation."""
        equation = sympy.Eq(
            sympy.MatrixSymbol(variable, *expr.shape), self._sym_matrix_replace(expr)
        )

        return equation

    def generate_code(self, equations, language, rewrite_heaviside=False):
        """Generate the wanted equations in a language of choice.

        :param equations: Which equation types should be generated?
        {'dydt', 'observables', 'jacobian', 'flux', 'sensitivity', 'flux_sensitivity'}
        :type equations: set[str]
        :param language: Language to generate code for.
        One of {c, c89, c99, f, julia, octave, matlab}
        :type language: str
        :param rewrite_heaviside: Rewrite heaviside as piecewise function, with as much of
        the equation folded into the optional clause to avoid intermediate NaN results.
        :type heaviside: bool

        :returns: One or more (file name, file content) pairs (depending on the language).
        :rtype: list[tuple[str,str]]

        :raises ValueError: A ValueError is raised if either the equation is not defined or
        the output language is not supported.
        """
        # Use a lambda as a lazy reference so the derivatives will not be derived if not needed.
        mapping = {
            "dydt": lambda: self.dydt,
            "observables": lambda: sympy.Matrix(list(self.observables.values())),
            "jacobian": lambda: self.jacobian,
            "flux": lambda: self.fluxes,
            "sensitivity": lambda: self.sensitivity,
            "flux_sensitivity": lambda: self.flux_sensitivity,
        }

        x = []
        for i in equations:
            if i not in mapping:
                raise ValueError("Equation not defined: {}".format(i))
            else:
                eq = mapping[i]()
                if rewrite_heaviside and eq.has(sympy.Heaviside):
                    # First we define Heaviside(0) as 0, since this is not the default.
                    # Then we rewrite the Heaviside as a piecewise function instead,
                    # which is then folded to make sure that the nan-producing power is folded
                    # into the equation and is not executed on values <= 0.
                    eq = eq.replace(
                        sympy.Heaviside,
                        lambda arg: sympy.Heaviside(arg, 0).rewrite(sympy.Piecewise),
                    )
                    for xi in range(eq.shape[0]):
                        for yi in range(eq.shape[1]):
                            eq[xi, yi] = sympy.piecewise_fold(eq[xi, yi])
                x.append((i, (self._create_matrix_expression(i, eq))))

        if language.lower() == "matlab":
            language = "octave"
        elif language.lower() == "fortran":
            language = "f95"
        elif language.lower() == "rust":
            raise NotImplementedError(
                "Current sympy rust codegen implementation not functional."
            )

        from sympy.utilities.codegen import codegen

        return codegen(x, language=language, to_files=False)

    def to_jitcode_ODE(self, **jitcode_args):
        """Create a JiTCODE model from the SymbolicModel.

        Equations will be automatically mapped to jitcode symbols and parameters will be marked as such.
        If symengine is installed, the equations will be converted to symengine symbolics before
        being passed to jitcode.
        """
        import jitcode

        symbol_mapping = {
            m: jitcode.y(i) for i, m in enumerate(self.metabolite_symbols)
        }
        time_mapping = {self.t_symbol: jitcode.t}
        equations = self.dydt.xreplace(symbol_mapping).xreplace(time_mapping)
        try:
            import symengine
        except ImportError:
            pass
        else:
            equations = symengine.sympify(equations)
        ODE = jitcode.jitcode(
            equations, control_pars=self.parameter_symbols, **jitcode_args
        )
        return ODE

# SymbolicSBML
A python package to work with SBML models defining kinetic models. This packages aims to make it easier to manipulate kinetic models in Python, allowing for easy model construction and modification.

Highlights:
- Read/Write SBML models.
- Easily manipulate kinetic rate laws via `sympy` (symbolic python) expressions.
- Automatically create rate laws for metabolic models (Modular Rate Law, Mass Action Kinetics).
- Significantly speed up scipy ODE integration through automated Cython code generation.
- Easily generate derived functions such as the Jacobian.
- Generate code for ODE equations in different languages using `sympy` code generation (`Matlab/Octave/C/Fortan/Julia`)
- See `example.py` for a documented example.

## Requirements:
- Python 3.4+
- Numpy
- Sympy
- libSBML (python-libsbml)
- Cython (Optional, for Cython code generation.)
- JiTCODE (Optional, for JiTCODE code generation.)

## Installation:
- The easiest way to install `SymbolicSBML` is via pip:

  `pip install git+https://gitlab.com/wurssb/Modelling/symbolicsbml.git#egg=symbolicSBML`
- Alternatively:
    - Install `numpy`, `sympy` and `python-libsbml` via your prefered method.
    - Clone this repository: 
    
      `git clone https://gitlab.com/wurssb/Modelling/symbolicsbml`

    - Run `python setup.py install` to install the module.

## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details.
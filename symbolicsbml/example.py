import pathlib

import numpy as np
import sympy
import scipy

import symbolicSBML

# --- Creating, loading and saving models ---
# Create a new model
model = symbolicSBML.SymbolicModel.from_structure(
    metabolites=["A", "B", "C"],
    reactions=["r1", "r2"],
    # A <-> B, B <-> 2 C
    stoichiometry=np.array([[-1, 1, 0], [0, -1, 2]]).T,
    rate_law_type="mass action",
)

# Save the SBML model
path = pathlib.Path("test/example.xml")
path.parent.mkdir(exist_ok=True)
model.to_sbml(path)

# Load an exisiting model
model = symbolicSBML.SymbolicModel.from_sbml(path)

# Load the structure of existing model, but create new rate laws, modular rate laws in this case.
model = symbolicSBML.SymbolicModel.from_sbml_structure(
    path,
    rate_law_type="modular",
    sub_rate_law="PM",
    parametrisation="weg",
    substitutions='all'
)

# --- Investigating the model ---

# Print flux equation per reaction.
for reaction, flux in zip(model.reactions, model.fluxes):
    print(reaction)
    # print(flux)
    # This looks better:
    sympy.pprint(flux)
    # Can also print latex output. Does this render in Jupyter notebooks?
    # sympy.latex(flux)
    print()

# Print equation per state.
for state, eq in zip(model.metabolites, model.dydt):
    print("d{}/dt".format(state))
    sympy.pprint(eq)
    print()

# Some more properties to check
print(model.stoichiometry)
print(model.jacobian)
print(model.sensitivity)  # (dy/dp)
print(model.flux_sensitivity)  # (df/dp)

# Parameter values or initial concentrations can be accessed through the underlying SBML model.
# if they have been set.
model.sbml_model.parameter_values
model.sbml_model.initial_concentrations

# --- Modifying a model ---

# You can change the model, but keep in mind that the model is determined by:
#   1) The stoichiometry matrix
#   2) The flux equation for each reaction
#   3) The names of th e reactions, metabolites and parameters
# Make sure to keep everything in sync, and do not modify properties inferred
# from these 3 items such as dydt or the jacobian.
model.fluxes[0] = model.fluxes[0] * 2

# Also make sure to call these functions to verify correctness and fix the symbolic equations.
model.verify()
model.recreate_symbols()

# --- Code generation ---

# Generate code using sympy for various equations in different languages
model.generate_code({"dydt", "flux"}, "matlab")
model.generate_code({"jacobian"}, "julia")
model.generate_code({"sensitivity"}, "fortran")
model.generate_code({"flux_sensitivity"}, "c")

# We can also generate python executable ODE systems in different ways.
# The speed of generating the code trades off with the execution.
# Execution speed: JitCode > CythonODE > SympyODE > SympyLambdifyODE
# Warning: For larger systems compiling the ODE might easily take 1h+.

# Create a sympy based ODE system.
t = 0.0
p = np.ones(len(model.parameters)) / 100
state = np.ones(len(model.metabolites))
msympy = symbolicSBML.codegen.SympyODE(model)
sol0 = scipy.integrate.solve_ivp(
    y0=state, fun=msympy.dydt, jac=msympy.jacobian, t_span=(0, 100), args=(p,)
)

# You can also create a python system to run the ODE directly.
mlambdify = symbolicSBML.codegen.SympyLambdifyODE(model)

mlambdify.dydt(t, state, p)
sol1 = scipy.integrate.solve_ivp(
    y0=state, fun=mlambdify.dydt, jac=mlambdify.jacobian, t_span=(0, 100), args=(p,)
)

# Or generate a Cython module to do the same (but execute faster).
cython_ode_builder = symbolicSBML.codegen.CythonODEBuilder(model)
cython_ode_builder.build("./test/cython2")  # This might take a while...
mcython = cython_ode_builder.create()
mcython.update_parameters(p)
mcython.dydt(t, state)
sol2 = scipy.integrate.solve_ivp(
    y0=state, fun=mcython.dydt, jac=mcython.jacobian, t_span=(0, 100)
)

# You can also generate JiTCODE models easily.
mjit = model.to_jitcode_ODE()  # This might take a while...
mjit.set_integrator("RK45")
mjit.set_initial_value(state)
mjit.set_parameters(p)
sol3 = mjit.integrate(100)

# Let's compare the results
print("Final state of solution at t=100")
print("Sympy ODE         :", sol0.y[:, -1])
print("Sympy lambidfy ODE:", sol1.y[:, -1])
print("Cython ODE        :", sol2.y[:, -1])
print("JiTCODE ODE       :", sol3)

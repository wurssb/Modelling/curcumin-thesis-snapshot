import itertools
import pathlib
import re
import string

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats
import seaborn as sns

from symbolicSBML import Parameters
from AmiciResultWrapper import AmiciResultWrapper


def load_data_subset(path, limit_samples=None, limit_design=None, t_measured=None):
    # Load data and extract only the relevant bits.
    data = {}
    for path in sorted(path.iterdir()):
        match = re.match(r"s(?P<sample>\d+)e(?P<experiment>\d+).h5", path.name)
        if match is not None:
            sample = int(match.group("sample"))
            if limit_samples is not None and sample >= limit_samples:
                continue
            experiment = int(match.group("experiment"))
            if limit_design is not None and experiment not in limit_design:
                continue
            wrapper = AmiciResultWrapper.from_hdf(path)
            if t_measured is not None:
                wrapper.t_prime = t_measured
            data[sample, experiment] = wrapper
    return data


def FIM_combined(data):
    df = (
        pd.concat(
            {i: d.sy.loc[d.t_prime] for i, d in enumerate(data)}, names=["experiment"]
        )
        .unstack("parameter")
        .fillna(0)
    )
    return df.T.dot(df).droplevel(0, 0).droplevel(0, 1)


base_dir = pathlib.Path("results_exp_pred")
plot_dir = pathlib.Path("results/exp_design_run")
plot_dir.mkdir(exist_ok=True, parents=True)

# Setting to load a subset for prototyping.
limit_samples = None
limit_design = None

# Read in parameters
parameter_sets = pd.read_csv(base_dir / "parameters.csv", index_col=0)
n_samples = (
    len(parameter_sets)
    if not limit_samples
    else min(limit_samples, len(parameter_sets))
)

# Read in experimental design.
design_round_1 = pd.read_csv(
    base_dir / "design_round_1.csv", header=[0, 1], index_col=0
)
# The pulse is actually never triggered if t == 0
design_round_1.loc[:, pd.IndexSlice["addition", :]] = 0
design_round_2 = pd.read_csv(
    base_dir / "design_round_2.csv", header=[0, 1], index_col=0
)

# Use a subset of the data as "measured" time points
# Keep in mind they have to be a subset of the simulated points, if we do not
# want to interpolate or rerun the simulations
t_measured = pd.Series(np.linspace(0, 250, 6, endpoint=True), name="measurements")

data = load_data_subset(
    base_dir / "simulations", limit_samples, limit_design, t_measured
)
print("Finished loading data round 1")
data_round_2 = load_data_subset(
    base_dir / "simulations_2", limit_samples, limit_design, t_measured
)
print("Finished loading data round 2")
# # Visualize designs
# fig, ax = plt.subplots()
# sns.heatmap(
#     ((design > 0) | design.isna()).T,
#     square=True,
#     xticklabels=True,
#     yticklabels=True,
#     ax=ax,
#     cbar=False,
# )
# ax.set_xlabel("Experiment number")
# fig.set_size_inches(20, 5)
# fig.tight_layout()
# fig.savefig(plot_dir / "designs.png", dpi=300)

# Plot some examples
# for sample, exp in ((0, 17), (1, 17), (0, 18), (1, 18)):
#     d = data[sample, exp]
#     fig, ax = d.plot_sensitivity_correlation(cluster=False)
#     fig.set_size_inches(15, 15)
#     fig.tight_layout()
#     fig.savefig(plot_dir / "pcorr_{}_{}.png".format(sample, exp), dpi=300)

#     fig, ax = d.plot_FIM()
#     fig.set_size_inches(15, 15)
#     fig.tight_layout()
#     fig.savefig(plot_dir / "FIM_{}_{}.png".format(sample, exp), dpi=300)

max_length = len(design_round_1)
samples = list(range(n_samples))
trace = {}
combinations = itertools.chain.from_iterable(
    itertools.combinations(design_round_1.index, r + 1) for r in range(max_length)
)
for combination in combinations:
    trace[combination] = [
        np.trace(FIM_combined(data[sample, i] for i in combination))
        for sample in samples
    ]

trace = pd.DataFrame(trace).rename_axis(
    columns=["exp_{}".format(i) for i in range(max_length)], index="Sample"
)

base_experiment_names = dict(
    zip(range(len(design_round_1.index)), string.ascii_uppercase)
)

singles = (
    trace.loc[:, pd.IndexSlice[:, np.NaN, np.NaN, np.NaN, np.NaN, np.NaN]]
    .droplevel([1, 2, 3, 4, 5, 6], axis=1)
    .rename_axis("Experiment", axis=1)
)
singles.to_csv(base_dir / "singles_trace_df.csv")


# Singles only
# fig, ax = plt.subplots()
# sns.boxplot(
#     data=singles,
#     ax=ax,
# )
# sns.despine(fig)

# All combinations
FIM_limits = -1, 150
experiments = ~pd.isna(trace.columns.to_frame())
exp_count = experiments.sum(axis=1)
combinations_df = trace.T.assign(exp_count=exp_count)
combinations_df.index = trace.columns.to_list()
combinations_df = (
    combinations_df.set_index("exp_count", append=True)
    .stack()
    .reset_index()
    .set_axis(["experiment", "exp_count", "Sample", "Trace"], axis=1)
)

# Plot with only the best at each number of experiments.
best = trace[trace.mean().groupby(exp_count).idxmax()].T
index = [tuple(int(i) for i in j if not pd.isna(i)) for j in best.index.to_list()]

best = (
    best.reset_index(drop=True)
    .unstack()
    .reset_index()
    .set_axis(["Sample", "N_exp", "Trace"], axis=1)
)
best.loc[:, "N_exp"] = best.loc[:, "N_exp"] + 1

# fig, ax = plt.subplots()
# sns.lineplot(
#     data=best,
#     x="N_exp",
#     y="Trace",
#     ci="sd",
#     ax=ax,
# )
# ax.set_ylabel("Trace(FIM)")
# ax.set_xlim(best.N_exp.min(), best.N_exp.max())
# ax.set_xlabel("Optimal combination of experiments")
# ax.set_xticklabels(
#     ["n={}\n".format(i) + str(idx).strip("(),") for i, idx in enumerate(index, 1)],
#     rotation=360 - 45,
# )
# sns.despine(fig)
# fig.tight_layout()
# fig.savefig(plot_dir / "round_1_best.png", dpi=300)

presence = (
    trace.columns.to_frame()
    .reset_index(drop=True)
    .stack()
    .reset_index()
    .drop("level_1", axis=1)
    .set_axis(["combination", "experiment"], axis=1)
)

# Add the best mean/std in the background.
bg = best.groupby("N_exp").Trace.agg(["mean", "std"])
# Map index to right points on the axis
bg = bg.aggregate(lambda x: presence.groupby("combination").count().experiment.map(x))

# ------------------------------------------
def experiment_trace_plot(
    bg,
    combinations_df,
    presence,
    base_experiment_names,
    xlim,
    markersize,
    fontsizes,
    figsize,
    width_ratios,
):
    fig = plt.figure(figsize=figsize)
    spec = fig.add_gridspec(ncols=2, nrows=1, width_ratios=width_ratios)
    ax1 = fig.add_subplot(spec[1])
    ax2 = fig.add_subplot(spec[0], sharey=ax1)

    # Plot shaded area (mean+-std)
    # Slightly extend the bg line
    bg_y = bg.index.values.copy()
    bg_y[0] -= 2
    bg_y[-1] += 2
    ax1.fill_betweenx(
        y=bg_y,
        x1=bg["mean"] - bg["std"],
        x2=bg["mean"] + bg["std"],
        color="gray",
        alpha=0.2,
        zorder=-1,
    )
    # Plot mean
    ax1.plot(
        bg["mean"],
        bg_y,
        color="gray",
        zorder=0,
    )
    sns.boxplot(
        data=combinations_df,
        y="experiment",
        x="Trace",
        hue="exp_count",
        dodge=False,
        ax=ax1,
        palette="muted",
        # Skip outliers
        showfliers=False,
    )
    ax1.legend(frameon=False, title="Number of Experiments")
    ax1.set_xlabel("Information content", size=fontsizes[0])
    ax1.tick_params(axis="x", which="major", labelsize=fontsizes[2])
    ax1.set_yticklabels([])
    ax1.set_ylabel(None)
    ax1.set_xlim(*xlim)
    lgd = ax1.get_legend()
    lgd.get_title().set_fontsize(fontsizes[0])
    [text.set_fontsize(fontsizes[1]) for text in lgd.get_texts()]
    sns.despine(fig)

    colors = sns.color_palette("muted", len(design_round_1))
    ax2.scatter(
        presence.experiment,
        presence.combination,
        marker=".",
        s=markersize,
        c=exp_count.map(dict(zip(range(1, 1 + len(design_round_1)), colors)))
        .iloc[presence.combination]
        .values,
        clip_on=False,
    )

    ax2.yaxis.set_visible(False)
    ax2.set_xticks(sorted(presence.experiment.unique()))
    ax2.set_xticklabels(
        base_experiment_names[i] for i in sorted(presence.experiment.unique())
    )
    ax2.set_xlabel("Experiment", size=fontsizes[0])
    ax2.tick_params(axis="x", which="major", labelsize=fontsizes[2])
    ax2.set_axisbelow(True)
    ax2.grid(axis="both")
    ax2.set_xlim(*ax2.get_xlim()[::-1])
    ax2.set_ylim(bg.index.max() + 0.5, bg.index.min() - 0.5)
    sns.despine(ax=ax2, left=True)
    fig.tight_layout()
    return fig, ax1, ax2


fig, ax1, ax2 = experiment_trace_plot(
    bg,
    combinations_df,
    presence,
    base_experiment_names=base_experiment_names,
    xlim=FIM_limits,
    markersize=200,
    fontsizes=(16, 14, 12),
    figsize=(10, 18),
    width_ratios=(1, 9),
)
fig.savefig(plot_dir / "round_1_combinations.png", dpi=300)

# Make a zoomed in version of the single + double only.
exp_cutoff = 2
cutoff = bg.loc[bg["mean"].diff() > 0].index[exp_cutoff - 1] - 1
fig, ax1, ax2 = experiment_trace_plot(
    bg.loc[:cutoff],
    combinations_df.loc[combinations_df.exp_count <= 2],
    presence.loc[presence.combination < (cutoff + 1)],
    base_experiment_names=base_experiment_names,
    xlim=(-0.5, 80),
    markersize=300,
    fontsizes=(14, 12, 10),
    figsize=(8, 8),
    width_ratios=(2, 9),
)
fig.savefig(plot_dir / "round_1_combinations_zoom.png", dpi=300)

# ------------------------------------------

# Calculate FIM for each round 2 experiment.
max_length = 1
samples = list(range(n_samples))
trace_round_2 = {}
combinations = itertools.chain.from_iterable(
    itertools.combinations(design_round_2.index, r + 1) for r in range(max_length)
)
for combination in combinations:
    traces = []
    for sample in samples:
        try:
            traces.append(
                np.trace(FIM_combined(data_round_2[sample, i] for i in combination))
            )
        except KeyError:
            traces.append(np.NaN)
    trace_round_2[combination] = traces
    # try:
    #     trace_round_2[combination] = [
    #         np.trace(FIM_combined(data_round_2[sample, i] for i in combination))
    #         for sample in samples
    #     ]
    # except KeyError:
    #     trace_round_2[combination] = np.full(len(samples), np.NaN)

trace_round_2 = pd.DataFrame(trace_round_2).rename_axis(
    columns=["exp_{}".format(i) for i in range(max_length)], index="Sample"
)
pulse_trace_df = (
    trace_round_2.T.stack()
    .reset_index()
    .set_axis(["Experiment", "Sample", "Trace"], axis=1)
)
pulse_trace_df = pulse_trace_df.assign(
    T_pulse=design_round_2.loc[
        pulse_trace_df.Experiment
    ].parameter.event_t_addition.values,
    M_pulse=design_round_2.loc[pulse_trace_df.Experiment]
    .addition.idxmax(axis=1)
    .values,
    Base=pd.Series(
        [
            (design_round_1.initial == design_round_2.initial.loc[i])
            .all(axis=1)
            .idxmax()
            for i in design_round_2.index
        ]
    ).values[pulse_trace_df.Experiment],
)
pulse_trace_df.to_csv(base_dir / "pulse_trace_df.csv")
singles_df = singles.stack().reset_index().set_axis(["Sample", "Base", "Trace"], axis=1)
singles_df.to_csv(base_dir / "singles_trace_df.csv")

# Plot an example of the complex interactions
subset = pd.concat(
    [
        pulse_trace_df.loc[pulse_trace_df.T_pulse == 17.5].drop(
            ["Experiment", "T_pulse"], axis=1
        ),
        singles_df.assign(M_pulse="-"),
    ]
).rename(columns={"M_pulse": "Metabolite"})
# Switch to letter names to match other figures.
subset.loc[:, "Base"] = subset.loc[:, "Base"].replace(base_experiment_names)

fig, ax = plt.subplots()
sns.boxplot(
    data=subset,
    y="Base",
    x="Trace",
    hue="Metabolite",
    hue_order=["-", "CUA", "CAA", "FEA"],
    orient="h",
    dodge=True,
    showfliers=False,
    ax=ax,
)
ax.set_xlabel("Information content")  # , fontdict={'fontsize': 12})
ax.set_ylabel("Base experiment")  # , fontdict={'fontsize': 12})
ax.get_legend().set_frame_on(False)
ax.set_xlim(-1)
sns.despine(fig)
fig.set_size_inches(5, 8)
fig.tight_layout()
fig.savefig(plot_dir / "round_2_details.png", dpi=300)

from symbolicSBML import Parameters

parameters = pd.DataFrame(
    [Parameters.split(i) for i in data[0, 0].FIM.index.droplevel(0)],
    columns=["Type", "Metabolite", "Reaction", "Enzyme"],
    index=data[0, 0].FIM.index.droplevel(0),
)
groups = {
    "c": "Cofactor concentration",
    "km": "Michaelis-Menten constant",
    "kv": "Enzyme (rate)",
    "mu": "Chemical potential",
    "u": "Enzyme (rate)",
    "u_v": "Enzyme (rate)",
}
include = ("parameter", "reaction", "metabolite")
parameters.Reaction = parameters.Reaction.fillna(parameters.Enzyme)
parameters = parameters.assign(
    Group=parameters.Type.map(groups),
    Display=[
        Parameters.to_latex(
            parameter=i.Type,
            metabolite=i.Metabolite,
            reaction=i.Reaction,
            include=include,
        )
        for i in parameters.itertuples()
    ],
)

# We take best example for a better look and contrast it with the same scenario w/o pulse.
base_experment = 0
t_pulse = 17.5 # Might need to change this depending on the outcome of the simulations, as it is dynamically set!
pulse_exp = design_round_2.index[
    (design_round_2.initial == design_round_1.loc[0, "initial"]).all(axis=1)
    & (design_round_2.addition.CUA == 2)
    & (design_round_2.parameter.event_t_addition == 15)
]
pulse_exp = pulse_exp[0]

# Investigate FIM properties for singles vs pulses
diag_feed = np.array(
    [np.diag(i.FIM) for (s, e), i in data.items() if e == base_experment]
)
diag_pulse = np.array(
    [np.diag(i.FIM) for (s, e), i in data_round_2.items() if e == pulse_exp]
)

diagonals = {}
for s in range(n_samples):
    if (s, base_experment) in data:
        diagonals["Feed", s] = np.diag(data[s, base_experment].FIM)
    if (s, base_experment) in data_round_2:
        diagonals["Pulse", s] = np.diag(data_round_2[s, base_experment].FIM)
diagonals = (
    pd.DataFrame(diagonals, index=parameters.index)
    .unstack()
    .reset_index()
    .set_axis(["Experiment", "Sample", "Parameter", "Value"], axis=1)
    .join(parameters.Display, on="Parameter")
)

n = 15
selection = "ks"
split = True
if selection == "top":
    p = (
        diagonals.groupby(["Experiment", "Parameter"])
        .Value.median()
        .groupby("Experiment")
        .nlargest(n)
        .index.get_level_values("Parameter")
        .unique()
    )
elif selection == "diff":
    p = (
        diagonals.groupby(["Parameter", "Experiment"])
        .Value.median()
        .unstack("Experiment")
        .diff(axis=1)
        .iloc[:, -1]
        .abs()
        .nlargest(n)
        .index
    )
elif selection == "ks":
    # Select largest difference through a two-sample Kolmogorov-Smirnov test.
    df = diagonals.pivot(
        index="Sample", columns=["Experiment", "Parameter"], values="Value"
    )

    ks = {}
    for col in df.Feed.columns.union(df.Pulse.columns):
        ks[col] = scipy.stats.ks_2samp(df.Feed[col], df.Pulse[col])
    ks = pd.DataFrame(ks, index=["statistic", "pvalue"]).T
    p = ks.nlargest(n, "statistic").index

if split:
    large = diagonals[diagonals.Parameter.isin(p)].groupby("Parameter").Value.mean() > 1
    large_p = large.index[large]
    small_p = large.index[~large]

    fig, (ax1, ax2) = plt.subplots(
        2, 1, gridspec_kw={"height_ratios": [len(large_p), len(small_p)]}
    )

    sns.boxplot(
        data=diagonals.loc[diagonals.Parameter.isin(large_p)],
        order=parameters.loc[large_p].Display,
        y="Display",
        x="Value",
        hue="Experiment",
        dodge=True,
        ax=ax1,
        palette="muted",
        showfliers=False,
    )
    ax1.tick_params(axis="x", which="major", labelsize=8)
    ax1.set_xlim(-0.2, 20)
    ax1.set_ylabel(None)
    ax1.set_xlabel(None)
    ax1.get_legend().remove()

    sns.boxplot(
        data=diagonals.loc[diagonals.Parameter.isin(small_p)],
        order=parameters.loc[small_p].Display,
        y="Display",
        x="Value",
        hue="Experiment",
        dodge=True,
        ax=ax2,
        palette="muted",
        showfliers=False,
    )
    ax2.tick_params(axis="x", which="major", labelsize=8)
    ax2.set_ylabel(None)
    ax2.set_xlim(-0.02, 2)
    ax2.set_xlabel("Information content")
    ax2.get_legend().set_frame_on(False)

    sns.despine(fig)
    fig.tight_layout()
    fig.subplots_adjust(hspace=0.12)
    fig.savefig(
        plot_dir / "example_dist_per_parameter_{}.png".format(selection), dpi=300
    )
else:

    fig, ax = plt.subplots()
    sns.boxplot(
        data=diagonals.loc[diagonals.Parameter.isin(p)],
        order=parameters.loc[p].Display,
        y="Display",
        x="Value",
        hue="Experiment",
        dodge=True,
        ax=ax,
        palette="muted",
        showfliers=False,
    )
    ax.tick_params(axis="y", which="major", labelsize=8)
    ax.set_ylabel(None)
    ax.set_xlabel("Information content")
    # ax.set_title("Top Parameters")
    ax.get_legend().set_frame_on(False)
    sns.despine(fig)
    fig.tight_layout()
    fig.savefig(
        plot_dir / "example_dist_per_parameter_{}.png".format(selection), dpi=300
    )
